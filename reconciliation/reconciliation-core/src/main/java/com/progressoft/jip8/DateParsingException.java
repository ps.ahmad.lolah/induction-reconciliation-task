package com.progressoft.jip8;

public class DateParsingException extends RuntimeException {
    public DateParsingException( String message ) {
        super(message);
    }
}
