package com.progressoft.jip8;

public class FileReadingException extends RuntimeException {
    public FileReadingException( String message, Exception e ) {
        super(message, e);
    }
}
