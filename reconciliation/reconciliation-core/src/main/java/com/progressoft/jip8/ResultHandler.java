package com.progressoft.jip8;

import java.io.Closeable;

// Visitor design pattern
public interface ResultHandler extends Closeable {

    void handleMatch(Record source );

    void handleMissing(Record record, String from );

    void handleMisMatch(Record source, Record target );
}
