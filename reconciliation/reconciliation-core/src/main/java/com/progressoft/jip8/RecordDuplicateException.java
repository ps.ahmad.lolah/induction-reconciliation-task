package com.progressoft.jip8;

public class RecordDuplicateException extends RuntimeException {
    public RecordDuplicateException( String message ) {
        super(message);
    }
}
