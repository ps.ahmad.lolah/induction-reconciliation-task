package com.progressoft.jip8;


import com.progressoft.jip8.validators.Validators;

@FunctionalInterface
public interface Filler<T> {
    Record getRecordFrom( T data, Validators<T> validators );

}
