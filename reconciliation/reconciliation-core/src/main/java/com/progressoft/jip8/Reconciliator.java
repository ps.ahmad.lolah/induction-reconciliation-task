package com.progressoft.jip8;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class Reconciliator {
    private RecordsService source;
    private RecordsService target;
    private ResultHandler resultHandler;

    // TODO enhance: pass those as parameters for the reconcile method
    public Reconciliator( ResultHandler resultHandler, RecordsService source, RecordsService target ) {
        Objects.requireNonNull(resultHandler, "null viewer");
        Objects.requireNonNull(source, "null record Service");
        Objects.requireNonNull(target, "null record Service");
        this.source = source;
        this.target = target;
        this.resultHandler = resultHandler;
    }

    public void reconcile() throws IOException {
        HashMap<String, Record> sourceRecords = new HashMap<>();

        readSourceFileRecord(sourceRecords);

        compareTargetWithSource(sourceRecords);

        sourceRecords
                .values()
                .forEach(r -> resultHandler.handleMissing(r, "SOURCE,"));

        resultHandler.close();
    }

    private void compareTargetWithSource( HashMap<String, Record> sourceRecords ) {
        Set<String> targetTransactionIds = new HashSet<>();

        for (Record targetRecord : target) {
            String key = targetRecord.getKEY();
            Record sourceRecord = sourceRecords.remove(key);
            compareAndMatch(targetRecord, sourceRecord);
            if (!targetTransactionIds.add(key))
                throw new RecordDuplicateException("duplicate record: " + key + " in target");
        }
    }

    private void compareAndMatch( Record targetRecord,
                                  Record sourceRecord ) {
        if (sourceRecord == null) {
            resultHandler.handleMissing(targetRecord, "TARGET,");
        } else if (!sourceRecord.equals(targetRecord)) {
            resultHandler.handleMisMatch(sourceRecord, targetRecord);
        } else {
            resultHandler.handleMatch(sourceRecord);

        }


    }

    private void readSourceFileRecord( HashMap<String, Record> sourceRecords ) {
        for (Record record : source) {
            String key = record.getKEY();
            if (sourceRecords.put(key, record) != null)
                throw new RecordDuplicateException("duplicate record: " + key + " in source");
        }
    }
}
