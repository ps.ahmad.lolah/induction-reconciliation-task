package com.progressoft.jip8;


public interface Record {
    String getKEY();

    Object format();

    void setFoundIn( String source );
    String getFoundIn();
}
