package com.progressoft.jip8;

public class ReconciliatorTest {
//    final String[] match = new String[1];
//    final String[] mismatch = new String[1];
//    final String[] missing = new String[4];
//
//    RecordsService DuplicateRecordService = new RecordsService() {
//        @Override
//        public Iterator<Record> iterator() {
//            return new Iterator<Record>() {
//                int count = 1;
//
//                @Override
//                public boolean hasNext() {
//                    return count <= 3;
//                }
//
//                @Override
//                public Record next() {
//                    Record jod = new Record() {
//                        @Override
//                        public String getKEY() {
//                            return "TR ";
//                        }
//
//                        @Override
//                        public String format() {
//                            return "TR ,156" + count + ",JOD,2020-2-20";
//                        }
//                    };
//                    count++;
//                    return jod;
//                }
//            };
//        }
//    };
//    RecordsService sourceService = new RecordsService() {
//        @Override
//        public Iterator<Record> iterator() {
//            return new Iterator<Record>() {
//                int count = 1;
//
//                @Override
//                public boolean hasNext() {
//                    return count <= 3;
//                }
//
//                @Override
//                public Record next() {
//                    Record jod = new Record() {
//                        @Override
//                        public String getKEY() {
//                            return "TR" + count;
//                        }
//
//                        @Override
//                        public String format() {
//                            return "TR" + count + ",156,JOD,2020-2-20";
//                        }
//
//                        @Override
//                        public boolean equals( Object obj ) {
//                            return true;
//                        }
//
//                    };
//                    count++;
//                    return jod;
//                }
//            };
//        }
//    };
//    RecordsService targetService = new RecordsService() {
//        @Override
//        public Iterator<Record> iterator() {
//            return new Iterator<Record>() {
//                int count = 1;
//                int count2 = 1;
//
//                @Override
//                public boolean hasNext() {
//                    return count <= 9;
//                }
//
//                @Override
//                public Record next() {
//                    Record jod = new Record() {
//                        @Override
//                        public String getKEY() {
//                            return "TR" + count;
//                        }
//
//                        @Override
//                        public String format() {
//                            return "TR" + count + ",156" + count2 + ",JOD,2020-2-20";
//                        }
//
//
//                        @Override
//                        public boolean equals( Object obj ) {
//                            return true;
//                        }
//                    };
//                    count = count * 3;
//                    count2++;
//                    return jod;
//                }
//            };
//        }
//    };
//    ResultHandler resultHandler = new ResultHandler() {
//        int count = 0;
//
//        @Override
//        public void HandleMatch( Record source ) {
//            match[0] = source.format();
//        }
//
//        @Override
//        public void HandleMisMatch( Record source, Record target ) {
//            mismatch[0] = source.format() + target.format();
//        }
//
//        @Override
//        public void HandleMissing( Record record, String from ) {
//            missing[count++] = from + record.format();
//        }
//
//        @Override
//        public void close() throws IOException {
//
//        }
//    };
//
//    @Test
//    public void givenNullViewer_whenConstructing_thenThrowNullPointerException() {
//        NullPointerException exception = Assertions.assertThrows(NullPointerException.class, () -> new Reconciliator(null, sourceService, targetService));
//        Assertions.assertEquals("null viewer", exception.getMessage());
//    }
//
//    @Test
//    public void givenNullSourceService_whenConstructing_thenThrowNullPointerException() {
//        NullPointerException exception = Assertions.assertThrows(NullPointerException.class, () -> new Reconciliator(resultHandler, null, targetService));
//        Assertions.assertEquals("null record Service", exception.getMessage());
//    }
//
//    @Test
//    public void givenNullTargetService_whenConstructing_thenThrowNullPointerException() {
//        NullPointerException exception = Assertions.assertThrows(NullPointerException.class, () -> new Reconciliator(resultHandler, targetService, null));
//        Assertions.assertEquals("null record Service", exception.getMessage());
//    }
//
//
//    @Test
//    public void givenValidServiceAndViewer_whenReconcile_thenWorkAsExpected() throws IOException {
//
//        new Reconciliator(resultHandler, sourceService, targetService).reconcile();
//
//
////        Assertions.assertEquals("TR 1,1561,JOD,2020-2-20", match[0]);
////        Assertions.assertEquals("TR 3,1563,JOD,2020-2-20TR 3,1562,JOD,2020-2-20", mismatch[0]);
////        Assertions.assertEquals("TARGET,TR 9,1563,JOD,2020-2-20", missing[0]);
////        Assertions.assertEquals("SOURCE,TR 2,1562,JOD,2020-2-20", missing[1]);
//
//    }
//
//    @Test
//    public void givenDuplicateSource_whenReconcile_thenFail() {
//        RecordDuplicateException recordDuplicateException = Assertions.assertThrows(RecordDuplicateException.class, () -> new Reconciliator(resultHandler, DuplicateRecordService, targetService).reconcile());
//        Assertions.assertEquals("duplicate record: TR  in source", recordDuplicateException.getMessage());
//
//    }
//
//    @Test
//    public void givenDuplicateTarget_whenReconcile_thenFail() {
//        RecordDuplicateException recordDuplicateException = Assertions.assertThrows(RecordDuplicateException.class, () -> new Reconciliator(resultHandler, targetService, DuplicateRecordService).reconcile());
//        Assertions.assertEquals("duplicate record: TR  in target", recordDuplicateException.getMessage());
//
//    }
}
