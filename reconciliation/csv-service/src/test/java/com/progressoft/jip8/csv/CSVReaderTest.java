package com.progressoft.jip8.csv;

import com.progressoft.jip8.FileReadingException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CSVReaderTest {
    public static Path data;

    @BeforeAll
    public static void copySampleFile() throws IOException {
        try (InputStream is = CSVReaderTest.class.getResourceAsStream("/bank-transactions.csv")) {
            data = Files.createTempFile("data", ".csv");
            try (OutputStream os = Files.newOutputStream(data)) {
                int length;
                byte[] buffer = new byte[1024 * 8];
                while ((length = is.read(buffer)) > 0) {
                    os.write(buffer, 0, length);
                }
                os.flush();
            }
        }
    }


    @Test
    public void givenValidPath_whenRead_thenReturnRecord() throws IOException {

        CSVReader reader = new CSVReader(data);
        String expected = "trans unique id,trans description,amount,currecny,purpose,value date,trans type";

        Assertions.assertEquals(expected, reader.get());

    }

    @Test
    public void givenNullPath_whenConstruct_thenThrowNullPointerException() {

        NullPointerException exception = Assertions.assertThrows(NullPointerException.class, () -> new CSVReader(null));
        Assertions.assertEquals("null path", exception.getMessage());
    }

    @Test
    public void givenInvalidPath_whenConstruct_thenThrowFileNotFoundException() {
        Path path = Paths.get(".");
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> new CSVReader(path));
        Assertions.assertEquals(". is a directory", exception.getMessage());
    }

    @Test
    public void givenClosedReader_whenGet_thenFailed() throws IOException {
        CSVReader reader = new CSVReader(data);
        reader.close();
        FileReadingException fileReadingException = Assertions.assertThrows(FileReadingException.class, reader::get);

        Assertions.assertEquals("failed to read CSV file", fileReadingException.getMessage());
    }
}

