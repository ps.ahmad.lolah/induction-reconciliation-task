package com.progressoft.jip8.csv;

import com.progressoft.jip8.FileReadingException;
import com.progressoft.jip8.Record;
import com.progressoft.jip8.SimpleRecord;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Iterator;

public class CSVRecordsServiceTest {
    public static Path data;

    @BeforeAll
    public static void copySampleFile() throws IOException {
        try (InputStream is = CSVRecordsServiceTest.class.getResourceAsStream("/bank-transactions.csv")) {
            data = Files.createTempFile("data", ".csv");
            try (OutputStream os = Files.newOutputStream(data)) {
                int length;
                byte[] buffer = new byte[1024 * 8];
                while ((length = is.read(buffer)) > 0) {
                    os.write(buffer, 0, length);
                }
                os.flush();
            }
        }
    }

    @Test
    public void givenClosedReader_whenHasNext_thenFail() throws IOException {
        CSVRecordsService csvRecordsService = new CSVRecordsService(data, ( s, g ) -> new SimpleRecord.RecordBuilder()
                .setTransactionID("BlaBla")
                .setAmount(new BigDecimal("156"))
                .setCurrency("JOD")
                .setValueDate("2020-2-20")
                .setValidator(( a ) -> {
                })
                .setFormat(( r ) -> "BlaBla")
                .build(), ( x ) -> {
        });


        Iterator<Record> iterator = csvRecordsService.iterator();
        while (iterator.hasNext()) {
            iterator.next();
        }

        FileReadingException fileReadingException = Assertions.assertThrows(FileReadingException.class, iterator::hasNext);
        Assertions.assertEquals("failed to read CSV file", fileReadingException.getMessage());
    }

    @Test
    public void givenValidPath_whenRead_thenReturnRecord() throws IOException {
        CSVRecordsService csvRecordsService = new CSVRecordsService(data, ( s, v ) ->
                new SimpleRecord.RecordBuilder()
                        .setAmount(BigDecimal.valueOf(140))
                        .setTransactionID("TR-47884222201")
                        .setCurrency("USD")
                        .setValueDate("2020-01-20")
                        .setValidator(( q ) -> {
                        })
                        .setFormat(( r ) -> "TR-47884222201,140,USD,2020-01-20")
                        .build()
                , ( s ) -> {
        });
        String expected = "TR-47884222201,140,USD,2020-01-20";

        for (Record record : csvRecordsService) {
            Assertions.assertEquals(expected, record.format());
            break;
        }
    }

    @Test
    public void givenNullPath_whenConstructing_thenThrowNullPointerException() {
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class,
                () -> new CSVRecordsService(null, ( s, v ) -> null, ( s ) -> {
                }));
        Assertions.assertEquals("null path", exception.getMessage());
    }


    @Test
    public void givenNullCSVFiller_WhenConstructing_throwNullPointerException() {
        NullPointerException exception =
                Assertions.assertThrows(NullPointerException.class, () -> new CSVRecordsService((data), null, ( s ) -> {
                }));
        Assertions.assertEquals("null filler", exception.getMessage());

    }

    @Test
    public void givenNullValidator_WhenConstructing_throwNullPointerException() {
        NullPointerException exception =
                Assertions.assertThrows(NullPointerException.class, () -> new CSVRecordsService((data), ( s, v ) -> null, null));
        Assertions.assertEquals("null validators", exception.getMessage());

    }


}
