package com.progressoft.jip8.csv;

import com.progressoft.jip8.DateParsingException;
import com.progressoft.jip8.Record;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class BankTransactionsCSVFillerTest {
    private BankTransactionsCSVFiller record = new BankTransactionsCSVFiller();

    @Test
    public void givenInvalidDate_whenGetRecordFrom_thenThrowDateParsingException() {
        DateParsingException exception =
                Assertions.assertThrows(DateParsingException.class, () -> record.getRecordFrom("TR-47884222201,,140.00,USD,,2020-01/20", ( s ) -> {
                }));
        Assertions.assertEquals("failed to parse date", exception.getMessage());

    }/*
    @Test
    public void givenInvalidRecord_whenGetRecordFrom_thenThrowIllegalArgumentException()
    {
        IllegalArgumentException exception =
                Assertions.assertThrows(IllegalArgumentException.class, () -> record.getRecordFrom(" ", ( s ) -> {}));
        Assertions.assertEquals("invalid record:  ",exception.getMessage());

    }*/

    @Test
    public void givenNullValidator_whenGetRecordFrom_thenThrowNullPointerException() {
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class, () -> record.getRecordFrom("", null));
        Assertions.assertEquals("null validator", exception.getMessage());
    }

    @Test
    public void givenNullRecord_whenGetRecordFrom_thenThrowNullPointerException() {
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class, () -> record.getRecordFrom(null, ( s ) -> {
        }));
        Assertions.assertEquals("null data", exception.getMessage());
    }

    @Test
    public void givenValidDataAndValidator_whenGetRecordFrom_thenReturnRecord() {
        String data = "TR-47884222201,Bla,140,USD,Bla,2020-05-12,Bla";
        Record recordFrom = record.getRecordFrom(data, ( s ) -> {
        });

        Assertions.assertEquals(recordFrom.getClass().getSimpleName(), "SimpleRecord");
    }

}
