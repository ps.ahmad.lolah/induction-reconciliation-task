package com.progressoft.jip8.csv;

import com.progressoft.jip8.DateParsingException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CSVRecordValidatorTest {
    @Test
    public void givenNullDateValidator_whenConstructing_thenThrowNullPointerException() {
        NullPointerException exception = assertThrows(NullPointerException.class, () -> new CSVRecordValidator.RecordValidatorBuilder()
                .setDateValidator(null, 5)
                .setAmountValidator(( r ) -> true, 2)
                .setColumnValidator(r -> true)
                .build());

        Assertions.assertEquals("null date validator", exception.getMessage());
    }

    @Test
    public void givenNullAmountValidator_whenConstructing_thenThrowNullPointerException() {
        NullPointerException exception = assertThrows(NullPointerException.class, () -> new CSVRecordValidator
                .RecordValidatorBuilder()
                .setDateValidator(( d ) -> true, 5)
                .setAmountValidator(null, 2)
                .setColumnValidator(r -> true)
                .build());

        Assertions.assertEquals("null amount validator", exception.getMessage());
    }

    @Test
    public void givenNullColumnValidator_whenConstructing_thenThrowNullPointerException() {
        NullPointerException exception = assertThrows(NullPointerException.class, () -> new CSVRecordValidator
                .RecordValidatorBuilder()
                .setDateValidator(( d ) -> true, 5)
                .setAmountValidator(r -> true, 2)
                .setColumnValidator(null)
                .build());

        Assertions.assertEquals("null column validator", exception.getMessage());
    }

//    @Test
//    public void givenValidRecord_whenValidate_thenReturnTrue() {
//        String validRecord = "TR-47884222201,online transfer,140,USD,donation,2020-01-20,D";
//
//
//        RecordValidator validator = new RecordValidator
//                .RecordValidatorBuilder()
//                .setDateValidator(( r ) -> true, 5)
//                .setAmountValidator(( r ) -> true, 2)
//                .setColumnValidator(r -> true)
//                .build();
//
//        Assertions.assertTrue(validator.validate(validRecord));
//    }


    @Test
    public void givenInvalidRecordLength_whenValidate_thenThrowIllegalArgumentException() {
        String invalidRecord = "TR-47884222201,online transfer,140,USD,donation,2020-01-20,D,102";

        CSVRecordValidator validator = new CSVRecordValidator
                .RecordValidatorBuilder()
                .setDateValidator(( r ) -> true, 5)
                .setAmountValidator(( r ) -> true, 2)
                .setColumnValidator(r -> false)
                .build();


        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                () -> validator.validate(invalidRecord));

        assertEquals("invalid record: " + invalidRecord, exception.getMessage());
    }

    @Test
    public void givenInvalidDateColumn_whenValidate_thenThrowIllegalArgumentException() {
        String invalidDateRecord = "TR-47884222201,online transfer,140,USD,donation,BlaBlaBla,D";

        CSVRecordValidator validator = new CSVRecordValidator
                .RecordValidatorBuilder()
                .setDateValidator(r -> false, 5)
                .setAmountValidator(( r ) -> true, 2)
                .setColumnValidator(( r ) -> true)
                .build();


        DateParsingException exception = assertThrows(DateParsingException.class,
                () -> validator.validate(invalidDateRecord));
        assertEquals("invalid date in record: " + invalidDateRecord, exception.getMessage());
    }


    @Test
    public void givenInvalidAmountColumn_whenValidate_thenThrowIllegalArgumentException() {
        String invalidAmountRecord = "TR-47884222201,online transfer,AAAAA,USD,donation,2020-01-20,D";

        CSVRecordValidator validator = new CSVRecordValidator
                .RecordValidatorBuilder()
                .setDateValidator(( r ) -> true, 5)
                .setAmountValidator(( r ) -> false, 2)
                .setColumnValidator(( r ) -> true)
                .build();


        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                () -> validator.validate(invalidAmountRecord));
        assertEquals("invalid amount in record: " + invalidAmountRecord, exception.getMessage());
    }
}
