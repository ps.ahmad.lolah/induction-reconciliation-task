package com.progressoft.jip8.csv;


import com.progressoft.jip8.FileReadingException;
import com.progressoft.jip8.Filler;
import com.progressoft.jip8.Record;
import com.progressoft.jip8.RecordsService;
import com.progressoft.jip8.validators.Validators;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;

public class CSVRecordsService implements RecordsService {
    private Path path;
    private Validators<String> validators;
    private CSVReader reader;
    private Filler<String> filler;

    public CSVRecordsService( Path path, Filler<String> filler, Validators<String> validators ) {
        Objects.requireNonNull(path, "null path");
        Objects.requireNonNull(filler, "null filler");
        Objects.requireNonNull(validators, "null validators");

        this.validators = validators;
        this.path = path;
        this.filler = filler;

    }

    private void skipHeader() {
        reader.get();
    }

    @Override
    public Iterator<Record> iterator() {
        try {
            reader = new CSVReader(path);
            skipHeader();
        } catch (IOException e) {
            throw new FileReadingException("failed to read CSV file", e);
        }

        return new Iterator<Record>() {
            String record;

            @Override
            public boolean hasNext() {
                record = reader.get();
                boolean hasNext = record != null;
                if (!hasNext) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        throw new FileReadingException("failed to close file", e);
                    }
                }
                return hasNext;
            }

            @Override
            public Record next() {
                if (record == null)
                    throw new NoSuchElementException();
                validators.validate(record);
                return filler.getRecordFrom(record, ( s ) -> {
                });
            }
        };
    }
}

