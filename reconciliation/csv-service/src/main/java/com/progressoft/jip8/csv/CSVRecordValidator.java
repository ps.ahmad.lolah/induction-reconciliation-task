package com.progressoft.jip8.csv;

import com.progressoft.jip8.DateParsingException;
import com.progressoft.jip8.validators.AmountValidator;
import com.progressoft.jip8.validators.ColumnValidator;
import com.progressoft.jip8.validators.DateValidator;
import com.progressoft.jip8.validators.Validators;

import java.util.Objects;

public class CSVRecordValidator implements Validators<String> {
    private ColumnValidator columnValidator;
    private DateValidator dateValidator;
    private AmountValidator amountValidator;
    private int dateIndex;
    private int amountIndex;

    private CSVRecordValidator( RecordValidatorBuilder builder ) {
        this.columnValidator = builder.columnValidator;
        this.dateValidator = builder.dateValidator;
        this.amountValidator = builder.amountValidator;
        this.amountIndex = builder.amountIndex;
        this.dateIndex = builder.dateIndex;
    }

    private String getAmount( String record ) {
        return record.split(",")[amountIndex];
    }

    private String getDate( String record ) {
        return record.split(",")[dateIndex];
    }

    @Override
    public void validate( String record ) {
        validateColumn(record);

        String date = getDate(record);
        validateDate(record, date);

        String amount = getAmount(record);
        validateAmount(record, amount);

    }

    private void validateAmount( String record, String amount ) {
        if (!amountValidator.isValid(amount))
            throw new IllegalArgumentException("invalid amount in record: " + record);
    }

    private void validateDate( String record, String date ) {
        if (!dateValidator.isValid(date))
            throw new DateParsingException("invalid date in record: " + record);
    }

    private void validateColumn( String record ) {
        if (!columnValidator.isValid(record))
            throw new IllegalArgumentException("invalid record: " + record);
    }

    public static class RecordValidatorBuilder {
        private ColumnValidator columnValidator;
        private DateValidator dateValidator;
        private AmountValidator amountValidator;
        private int dateIndex;
        private int amountIndex;

        public RecordValidatorBuilder setColumnValidator( ColumnValidator columnValidator ) {
            this.columnValidator = columnValidator;
            return this;
        }

        public RecordValidatorBuilder setDateValidator( DateValidator dateValidator, int dateIndex ) {
            this.dateValidator = dateValidator;
            this.dateIndex = dateIndex;
            return this;
        }

        public RecordValidatorBuilder setAmountValidator( AmountValidator amountValidator, int amountIndex ) {
            this.amountValidator = amountValidator;
            this.amountIndex = amountIndex;
            return this;
        }

        public CSVRecordValidator build() {
            CSVRecordValidator CSVRecordValidator = new CSVRecordValidator(this);
            validateRecordValidatorObject(CSVRecordValidator);
            return CSVRecordValidator;
        }

        private void validateRecordValidatorObject( CSVRecordValidator CSVRecordValidator ) {
            Objects.requireNonNull(CSVRecordValidator.columnValidator, "null column validator");
            Objects.requireNonNull(CSVRecordValidator.amountValidator, "null amount validator");
            Objects.requireNonNull(CSVRecordValidator.dateValidator, "null date validator");
        }
    }
}
