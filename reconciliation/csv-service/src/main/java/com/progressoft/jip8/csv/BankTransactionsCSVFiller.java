package com.progressoft.jip8.csv;


import com.progressoft.jip8.DateParsingException;
import com.progressoft.jip8.Filler;
import com.progressoft.jip8.Record;
import com.progressoft.jip8.SimpleRecord;
import com.progressoft.jip8.validators.Validators;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Currency;
import java.util.Objects;

public class BankTransactionsCSVFiller implements Filler<String> {
    @Override
    public Record getRecordFrom( String data, Validators<String> validators ) {
        Objects.requireNonNull(validators, "null validator");
        Objects.requireNonNull(data, "null data");

        validators.validate(data);

        return getNewRecord(data);
    }

    private Record getNewRecord( String data ) {

        String[] split = data.split(",");
        String currencyCode = split[3];

        String reference = split[0];
        String date = getDate(split[5]);

        int currencyFraction = getDefaultFractionDigits(currencyCode);
        BigDecimal amount = new BigDecimal(split[2]).setScale(currencyFraction);

        return getRecord(currencyCode, reference, date, amount);
    }

    private int getDefaultFractionDigits( String currencyCode ) {
        return Currency.getInstance(currencyCode).getDefaultFractionDigits();
    }

    private SimpleRecord getRecord( String currencyCode, String reference, String date, BigDecimal amount ) {
        return new SimpleRecord.RecordBuilder()
                .setTransactionID(reference)
                .setAmount(amount)
                .setCurrency(currencyCode)
                .setValueDate(date)
                .setValidator(( s ) -> {
                })
                .setFormat(( r ) -> reference + "," + currencyCode + "," + amount + "," + date)
                .build();
    }

    private String getDate( String date ) {
        SimpleDateFormat from = new SimpleDateFormat("yyyy-mm-dd");
        try {
            return from.format(from.parse(date));
        } catch (ParseException e) {
            throw new DateParsingException("failed to parse date");
        }
    }

}
