package com.progressoft.jip8.csv;

import com.progressoft.jip8.FileReadingException;
import com.progressoft.jip8.validators.FileValidator;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;
import java.util.function.Supplier;

public class CSVReader implements Supplier<String>, Closeable {
    private BufferedReader br;


    public CSVReader( Path path ) throws IOException {
        Objects.requireNonNull(path, "null path");
        FileValidator.isValid(path.toAbsolutePath(), "CSV");

        br = Files.newBufferedReader(path);
    }

    @Override
    public String get() {
        try {
            String s = br.readLine();
            return s;
        } catch (IOException e) {
            throw new FileReadingException("failed to read CSV file", e);
        }
    }

    @Override
    public void close() throws IOException {
        br.close();
    }
}
