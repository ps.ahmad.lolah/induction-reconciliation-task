package com.progressoft.jip8.validators;

@FunctionalInterface
public interface Validator {
    boolean isValid( String record );
}
