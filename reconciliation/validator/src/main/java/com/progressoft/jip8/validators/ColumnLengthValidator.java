package com.progressoft.jip8.validators;

import java.util.Objects;

public class ColumnLengthValidator implements ColumnValidator {
    private int recordLength;

    public ColumnLengthValidator( int length ) {
        if (length < 1)
            throw new IllegalArgumentException("invalid num of column");
        this.recordLength = length;


    }

    @Override
    public boolean isValid( String record ) {
        Objects.requireNonNull(record, "null record");
        if (isValidRecord(record))
            return false;
        return true;
    }

    private boolean isValidRecord( String record ) {
        return record.split(",").length != recordLength;
    }
}
