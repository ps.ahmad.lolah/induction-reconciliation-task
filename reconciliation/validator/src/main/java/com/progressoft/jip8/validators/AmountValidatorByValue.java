package com.progressoft.jip8.validators;

public class AmountValidatorByValue implements AmountValidator {
    private String syntax =
            "((([1-9][0-9]*)|)((\\.[0-9]+)|))";

    public boolean isValid( String record ) {
        if (record == null || record.equals(""))
            throw new NullPointerException("null amount");

        boolean matches = record.matches(syntax);
        if (!matches)
            return false;

        return true;
    }

}
