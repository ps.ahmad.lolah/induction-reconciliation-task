package com.progressoft.jip8.validators;

public interface Validators<T> {
    void validate( T record );
}
