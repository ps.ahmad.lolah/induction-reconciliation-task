package com.progressoft.jip8.validators;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Objects;

public class DateValidatorUsingDateFormat implements DateValidator {
    private String dateFormat;
    private DateFormat sdf;

    public DateValidatorUsingDateFormat( String dateFormat ) {
        Objects.requireNonNull(dateFormat, "null date format");
        this.dateFormat = dateFormat;
        sdf = new SimpleDateFormat(this.dateFormat);
        sdf.setLenient(false);
    }


    public boolean isValid( String record ) {
        Objects.requireNonNull(record, "null date");
        try {
            sdf.parse(record);
        } catch (ParseException e) {
            return false;
        }
        return true;
    }
}
