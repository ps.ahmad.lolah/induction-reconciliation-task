package com.progressoft.jip8.validators;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;

public class FileValidator {

    public static Boolean isValid( Path path, String extension ) throws FileNotFoundException {
        checkIfValid(path);

        if (!areIdentical(path, extension)) {
            String[] paths = String.valueOf(path.toAbsolutePath()).split(File.separator);
            throw new IllegalArgumentException("(" + paths[paths.length - 1] + ") is not a " + extension + " file");
        }

        return true;
    }

    private static boolean areIdentical( Path path, String extension ) {
        boolean b = String.valueOf(path.toAbsolutePath()).endsWith(extension.toLowerCase());
        return b;
    }

    private static void checkIfValid( Path path ) throws FileNotFoundException {
        Objects.requireNonNull(path, "null path");
        if (!Files.exists((path)))
            throw new FileNotFoundException();
        if (Files.isDirectory(path)) {
            String[] paths = String.valueOf(path.toAbsolutePath()).split(File.separator);
            throw new IllegalArgumentException(paths[paths.length - 1] + " is a directory");
        }
    }


}
