package com.progressoft.jip8.validators;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileValidatorTest {

    @Test
    public void givenValidPathAndExtension_whenISValid_thenReturnTrue() throws IOException {
        Path test = Files.createTempFile("Test", ".csv");
        Assertions.assertTrue(FileValidator.isValid(test, "CSV"));
    }

    @Test
    public void givenNullPAth_whenConstruct_thenThrowNullPointerException() {
        NullPointerException exception =
                Assertions.assertThrows(NullPointerException.class, ()
                        -> FileValidator.isValid(null, "json"));
        Assertions.assertEquals("null path", exception.getMessage());

    }

    @Test
    public void givenInvalidPath_whenConstruct_thenThrowFileNotFoundException() {
        Assertions.assertThrows(FileNotFoundException.class,
                () -> FileValidator.isValid(Paths.get("./Abc"), "josn"));
    }

    @Test
    public void givenDirectoryPath_whenConstruct_thenThrowIllegalArgumentException() {
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> FileValidator.isValid(Paths.get("./"), "josn"));

        Assertions.assertEquals(". is a directory", exception.getMessage());
    }

    @Test
    public void givenNotIdenticalPathAndExtension_whenIsValid_thenThrowIllegalArgumentException() throws IOException {
        Path path = Files.createTempFile("data", ".csv");
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> FileValidator.isValid(path, "josn"));

        Assertions.assertEquals("(" + path.getFileName() + ") is not a josn file", exception.getMessage());
    }
}