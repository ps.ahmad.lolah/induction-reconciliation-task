package com.progressoft.jip8.validators;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class DateValidatorUsingDateFormatTest {


    @Test
    public void givenNullFormat_whenConstruct_thenThrowNullPointerException() {
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class, () -> new DateValidatorUsingDateFormat(null));
        Assertions.assertEquals("null date format", exception.getMessage());

    }

    @Test
    public void givenNullDate_whenValidate_thenThrowNullPointerException() {
        DateValidatorUsingDateFormat validator = new DateValidatorUsingDateFormat("yyyy-mm-dd");
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class, () -> validator.isValid(null));
        Assertions.assertEquals("null date", exception.getMessage());

    }

    @Test
    public void givenInvalidDate_whenValidate_thenReturnFalse() {
        DateValidatorUsingDateFormat dateFormat = new DateValidatorUsingDateFormat("yyyy-mm-dd");

        Assertions.assertFalse(dateFormat.isValid("12-565-89"));
    }

    @Test
    public void givenValidDate_whenValidate_thenReturnTrue() {
        DateValidatorUsingDateFormat dateFormat = new DateValidatorUsingDateFormat("yyyy-mm-dd");
        Assertions.assertTrue(dateFormat.isValid("1994-12-21"));
    }
}
