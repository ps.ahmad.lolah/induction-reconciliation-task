package com.progressoft.jip8.validators;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class AmountValidatorByValueTest {

    @ParameterizedTest
    @ValueSource(strings = {"10", "15.0", "546.20"})
    public void givenValidAmount_whenValidate_thenReturnTrue( String amount ) {
        AmountValidatorByValue validator = new AmountValidatorByValue();
        Assertions.assertTrue(validator.isValid(amount));
    }

    @ParameterizedTest
    @ValueSource(strings = {"-10", "BlaBla", "bal10Bla"})
    public void givenInvalidAmount_whenValidate_thenReturnFalse( String amount ) {
        AmountValidatorByValue validator = new AmountValidatorByValue();
        Assertions.assertFalse(validator.isValid(amount));
    }

    @Test
    public void givenNullAmount_whenValidate_thenThrowNullPointerException() {
        AmountValidatorByValue validator = new AmountValidatorByValue();
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class, () -> validator.isValid(null));
        Assertions.assertEquals("null amount", exception.getMessage());
        exception = Assertions.assertThrows(NullPointerException.class, () -> validator.isValid(""));
        Assertions.assertEquals("null amount", exception.getMessage());
    }
}
