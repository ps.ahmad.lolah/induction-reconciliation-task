package com.progressoft.jip8.validators;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class ColumnLengthValidatorTest {
    @ParameterizedTest
    @ValueSource(ints = {0, -1, -6})
    public void givenInvalidNumOfCol_whenConstructing_thenThrowIllegalArgumentException( int invalidIndex ) {
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> new ColumnLengthValidator(-1));
        Assertions.assertEquals("invalid num of column", exception.getMessage());

    }

    @Test
    public void givenNullRecord_whenValidate_thenThrowNullPointerException() {
        ColumnLengthValidator validator = new ColumnLengthValidator(5);
        Assertions.assertThrows(NullPointerException.class, () -> validator.isValid(null));
    }

    @Test
    public void givenRecord_whenValidate_thenReturnExpectedResult() {
        ColumnLengthValidator validator = new ColumnLengthValidator(2);
        Assertions.assertTrue(validator.isValid("BlaBla,BlaBla"));
        Assertions.assertFalse(validator.isValid("BlaBla,BlaBla,BlaBla"));
    }
}
