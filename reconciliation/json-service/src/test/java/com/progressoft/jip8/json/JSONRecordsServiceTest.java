package com.progressoft.jip8.json;

import com.progressoft.jip8.Record;
import com.progressoft.jip8.RecordsService;
import com.progressoft.jip8.SimpleRecord;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;

public class JSONRecordsServiceTest {

    public static Path data;

    @BeforeAll
    public static void copySampleFile() throws IOException {
        try (InputStream is = JSONRecordsServiceTest.class.getResourceAsStream("/online-banking-transactions.json")) {
            data = Files.createTempFile("data", ".json");
            try (OutputStream os = Files.newOutputStream(data)) {
                int length;
                byte[] buffer = new byte[1024 * 8];
                while ((length = is.read(buffer)) > 0) {
                    os.write(buffer, 0, length);
                }
                os.flush();
            }
        }
    }

    @Test
    public void givenValidPath_whenRead_thenReturnRecord() throws IOException {
        RecordsService jsonRecordsService = new JSONRecordsService(data, ( s, v ) ->
                new SimpleRecord.RecordBuilder()
                        .setValueDate("2020-01-20")
                        .setCurrency("USD")
                        .setTransactionID("TR-47884222201")
                        .setAmount(BigDecimal.valueOf(140))
                        .setValidator(( q ) -> {
                        }).setFormat(( r ) -> "TR-47884222201,140,USD,2020-01-20")

                        .build()
                , ( s ) -> {
        });
        String expected = "TR-47884222201,140,USD,2020-01-20";

        for (Record record : jsonRecordsService) {
            Assertions.assertEquals(expected, record.format());
            break;
        }
    }

    @Test
    public void givenNullPath_whenConstructing_thenThrowNullPointerException() {
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class, () -> new JSONRecordsService(null, ( s, v ) -> null, ( s ) -> {
        }));
        Assertions.assertEquals("null path", exception.getMessage());
    }

    @Test
    public void givenNullCSVFiller_WhenConstructing_throwNullPointerException() {
        NullPointerException exception =
                Assertions.assertThrows(NullPointerException.class, () -> new JSONRecordsService(data, null, ( s ) -> {
                }));
        Assertions.assertEquals("null filler", exception.getMessage());
    }

    @Test
    public void givenNullValidator_WhenConstructing_throwNullPointerException() {
        NullPointerException exception =
                Assertions.assertThrows(NullPointerException.class, () -> new JSONRecordsService(data, ( s, v ) -> null, null));
        Assertions.assertEquals("null validators", exception.getMessage());
    }


}
