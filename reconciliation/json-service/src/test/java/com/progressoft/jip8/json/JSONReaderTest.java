package com.progressoft.jip8.json;

import com.progressoft.jip8.FileReadingException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class JSONReaderTest {

    public static Path data;

    @BeforeAll
    public static void copySampleFile() throws IOException {
        try (InputStream is = JSONReaderTest.class.getResourceAsStream("/online-banking-transactions.json")) {
            data = Files.createTempFile("data", ".json");
            try (OutputStream os = Files.newOutputStream(data)) {
                int length;
                byte[] buffer = new byte[1024 * 8];
                while ((length = is.read(buffer)) > 0) {
                    os.write(buffer, 0, length);
                }
                os.flush();
            }
        }
    }

    @Test
    public void givenCloseBufferedReader_whenRead_ThenFail() throws IOException {
        JSONReader jsonReader = new JSONReader(data);
        jsonReader.close();
        FileReadingException fileReadingException = Assertions.assertThrows(FileReadingException.class, jsonReader::get);
        Assertions.assertEquals("failed to read JSON file", fileReadingException.getMessage());
    }

    @Test
    public void givenValidPath_whenRead_thenReturnRecord() throws IOException {
        JSONReader reader = new JSONReader(data);
        String expected = "";

        //Assertions.assertEquals(expected, reader.get());
    }

    @Test
    public void givenInvalidPath_whenConstructing_throwNullPointerException() throws FileNotFoundException {
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class, () -> new JSONReader(null));
        Assertions.assertEquals("null path", exception.getMessage());

    }

    @Test
    public void givenInvalidPath_whenConstruct_thenThrowFileNotFoundException() {
        Path path = Paths.get(".");
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> new JSONReader(path));
        Assertions.assertEquals(". is a directory", exception.getMessage());
    }

}
