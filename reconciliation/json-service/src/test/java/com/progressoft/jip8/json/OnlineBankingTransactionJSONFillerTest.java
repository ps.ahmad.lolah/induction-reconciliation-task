package com.progressoft.jip8.json;

import org.json.simple.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class OnlineBankingTransactionJSONFillerTest {
    private OnlineBankingTransactionJSONFiller record = new OnlineBankingTransactionJSONFiller();

//    @Test
//    public void givenInvalidDateRecord_whenGetRecordFrom_thenThrowIllegalArgumentException()
//    {
//        DateParsingException exception =
//                Assertions.assertThrows(DateParsingException.class, () -> record.getRecordFrom("TR-47884222201,140.00,USD,2020-01/20", ( s ) -> {}));
//        Assertions.assertEquals("failed to parse date",exception.getMessage());
//
//    }

    @Test
    public void givenNullValidator_whenGetRecordFrom_thenThrowNullPointerException() {
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class, () -> record.getRecordFrom(new JSONObject(), null));
        Assertions.assertEquals("null validators", exception.getMessage());
    }

    @Test
    public void givenNullRecord_whenGetRecordFrom_thenThrowNullPointerException() {
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class, () -> record.getRecordFrom(null, ( s ) -> {
        }));
        Assertions.assertEquals("null data", exception.getMessage());
    }
//
//    @Test
//    public void givenValidDataAndValidator_whenGetRecordFrom_thenReturnRecord()
//    {
//        String data = "TR-47884222201,140,USD,20/05/2020";
//        Record recordFrom = record.getRecordFrom(data, ( s ) -> {});
//
//        Assertions.assertEquals(recordFrom.getClass().getSimpleName(),"Record");
//    }

}
