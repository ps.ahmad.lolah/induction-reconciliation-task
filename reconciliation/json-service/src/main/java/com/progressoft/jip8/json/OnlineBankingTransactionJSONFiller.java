package com.progressoft.jip8.json;


import com.progressoft.jip8.DateParsingException;
import com.progressoft.jip8.Filler;
import com.progressoft.jip8.Record;
import com.progressoft.jip8.SimpleRecord;
import com.progressoft.jip8.validators.Validators;
import org.json.simple.JSONObject;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Currency;
import java.util.Objects;

public class OnlineBankingTransactionJSONFiller implements Filler<JSONObject> {
    public Record getRecordFrom( JSONObject record, Validators<JSONObject> validators ) {
        Objects.requireNonNull(record, "null data");
        Objects.requireNonNull(validators, "null validators");
        validators.validate(record);

        String reference = (String) record.get("reference");
        String currencyCode = (String) record.get("currencyCode");
        String amount = (String) record.get("amount");
        String date = getDate((String) record.get("date"));

        int currencyFraction = Currency.getInstance(currencyCode).getDefaultFractionDigits();
        BigDecimal amount1 = new BigDecimal(amount).setScale(currencyFraction);

        SimpleRecord simpleRecord = new SimpleRecord.RecordBuilder()
                .setTransactionID(reference)
                .setAmount(amount1)
                .setCurrency(currencyCode)
                .setValueDate((date))
                .setValidator(( s ) -> {
                })
                .setFormat(( r ) -> reference + "," + currencyCode + "," + amount + "," + (date))
                .build();
        return simpleRecord;
    }

    private String getDate( String source ) {
        SimpleDateFormat from = new SimpleDateFormat("dd/mm/yyyy");
        SimpleDateFormat to = new SimpleDateFormat("yyyy-mm-dd");
        try {
            String date = to.format(from.parse(source));
            return date;
        } catch (ParseException e) {
            throw new DateParsingException("failed to parse date");
        }
    }


}
