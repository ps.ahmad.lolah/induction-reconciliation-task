package com.progressoft.jip8.json;

import com.progressoft.jip8.FileReadingException;
import com.progressoft.jip8.validators.FileValidator;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.Closeable;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Objects;
import java.util.function.Supplier;

public class JSONReader implements Supplier<JSONArray>, Closeable {
    private String path;
    private JSONParser jsonParser;
    private FileReader reader;

    public JSONReader( Path path ) throws FileNotFoundException {
        Objects.requireNonNull(path, "null path");
        FileValidator.isValid((path), "JSON");
        this.path = String.valueOf(path.toAbsolutePath());
        jsonParser = new JSONParser();
        reader = new FileReader(String.valueOf(path));
    }

    @Override
    public JSONArray get() {

        try {
            Object obj = jsonParser.parse(reader);
            return (JSONArray) obj;
        } catch (IOException | ParseException e) {
            throw new FileReadingException("failed to read JSON file", e);
        }
    }

    @Override
    public void close() throws IOException {
        reader.close();
    }
}
