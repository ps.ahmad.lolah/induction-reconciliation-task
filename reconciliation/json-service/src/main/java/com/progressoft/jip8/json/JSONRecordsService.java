package com.progressoft.jip8.json;

import com.progressoft.jip8.FileReadingException;
import com.progressoft.jip8.Filler;
import com.progressoft.jip8.Record;
import com.progressoft.jip8.RecordsService;
import com.progressoft.jip8.validators.Validators;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;

public class JSONRecordsService implements RecordsService {
    private Validators<JSONObject> validators;
    private Filler<JSONObject> filler;
    private Path path;

    public JSONRecordsService( Path path, Filler<JSONObject> filler, Validators<JSONObject> validators ) throws FileNotFoundException {
        Objects.requireNonNull(path, "null path");
        Objects.requireNonNull(filler, "null filler");
        Objects.requireNonNull(validators, "null validators");

        this.validators = validators;
        this.filler = filler;
        this.path = path;
    }

    @Override
    public Iterator<Record> iterator() {
        JSONArray TransactionList = getJSONArray();
        return new Iterator<Record>() {
            final int length = TransactionList.size();
            int index = 0;

            @Override
            public boolean hasNext() {
                return index < length;
            }

            @Override
            public Record next() {
                if (!hasNext())
                    throw new NoSuchElementException();

                JSONObject record = (JSONObject) TransactionList.get(index++);
                validators.validate(record);
                return filler.getRecordFrom(record, ( s ) -> {
                });
            }
        };
    }

    private JSONArray getJSONArray() {
        JSONArray TransactionList;
        try {
            JSONReader reader = new JSONReader(path);
            TransactionList = reader.get();

            reader.close();
        } catch (IOException e) {
            throw new FileReadingException("failed to close JSON file", e);
        }
        return TransactionList;
    }
}
