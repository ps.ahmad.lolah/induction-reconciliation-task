package com.progressoft.jip8.json;

import com.progressoft.jip8.DateParsingException;
import com.progressoft.jip8.validators.AmountValidator;
import com.progressoft.jip8.validators.DateValidator;
import com.progressoft.jip8.validators.Validators;
import org.json.simple.JSONObject;

public class JSONRecordValidator implements Validators<JSONObject> {
    private DateValidator dateValidator;
    private AmountValidator amountValidator;

    public JSONRecordValidator( DateValidator dateValidator, AmountValidator amountValidator ) {
        this.dateValidator = dateValidator;
        this.amountValidator = amountValidator;
    }

    @Override
    public void validate( JSONObject record ) {
        String amount = (String) record.get("amount");
        String date = (String) record.get("date");

        validateDate(record, date);

        validateAmount(record, amount);

    }

    private void validateAmount( JSONObject record, String amount ) {
        if (!amountValidator.isValid(amount))
            throw new IllegalArgumentException("invalid amount in record: " + record);
    }

    private void validateDate( JSONObject record, String date ) {
        if (!dateValidator.isValid(date))
            throw new DateParsingException("invalid date in record: " + record);
    }
}
