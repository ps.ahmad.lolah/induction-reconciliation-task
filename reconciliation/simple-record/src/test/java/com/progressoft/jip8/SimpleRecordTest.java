package com.progressoft.jip8;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

public class SimpleRecordTest {
    @Test
    public void givenNullTransactionID_whenBuild_thenThrowNullPointerException() {
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class, () -> new SimpleRecord.RecordBuilder()
                .setTransactionID(null)
                .setAmount(new BigDecimal("156"))
                .setCurrency("JOD")
                .setValueDate("2020-2-20")
                .setFormat(( r ) -> "BlaBla")
                .setValidator(( s ) -> {
                })
                .build());

        Assertions.assertEquals("null transaction id", exception.getMessage());
    }


    @Test
    public void givenNullCurrency_whenBuild_thenThrowNullPointerException() {
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class, () -> new SimpleRecord.RecordBuilder()
                .setTransactionID("BlaBla")
                .setAmount(new BigDecimal("156"))
                .setCurrency(null)
                .setValueDate("2020-2-20")
                .setValidator(( s ) -> {
                })
                .setFormat(( r ) -> "BlaBla")
                .build());

        Assertions.assertEquals("null currency", exception.getMessage());
    }


    @Test
    public void givenNullDate_whenBuild_thenThrowNullPointerException() {
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class, () -> new SimpleRecord.RecordBuilder()
                .setTransactionID("BlaBla")
                .setAmount(new BigDecimal("156"))
                .setCurrency("JOD")
                .setValueDate(null)
                .setValidator(( s ) -> {
                })
                .setFormat(( r ) -> "BlaBla")
                .build());

        Assertions.assertEquals("null date", exception.getMessage());
    }


    @Test
    public void givenNullValidator_whenBuild_thenThrowNullPointerException() {
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class, () -> new SimpleRecord.RecordBuilder()
                .setTransactionID("BlaBla")
                .setAmount(new BigDecimal("156"))
                .setCurrency("JOD")
                .setValueDate("2020-2-20")
                .setValidator(null)
                .setFormat(( r ) -> "BlaBla")
                .build());

        Assertions.assertEquals("null validators", exception.getMessage());
    }

    @Test
    public void givenNullFormat_whenBuild_thenThrowNullPointerException() {
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class, () -> new SimpleRecord.RecordBuilder()
                .setTransactionID("BlaBla")
                .setAmount(new BigDecimal("156"))
                .setCurrency("JOD")
                .setValueDate("2020-2-20")
                .setValidator(( s ) -> {
                })
                .build());

        Assertions.assertEquals("null format", exception.getMessage());
    }

    @Test
    public void givenRecord_whenHashCode_thenWorkAsExpected() {
        Record record = new SimpleRecord.RecordBuilder()
                .setTransactionID("BlaBla")
                .setAmount(new BigDecimal("156"))
                .setCurrency("JOD")
                .setValueDate("2020-2-20")
                .setValidator(( s ) -> {
                })
                .setFormat(( r ) -> "BlaBla")
                .build();

        Record record2 = new SimpleRecord.RecordBuilder()
                .setTransactionID("BlaBla")
                .setAmount(new BigDecimal("156"))
                .setCurrency("JOD")
                .setValueDate("2020-2-20")
                .setValidator(( s ) -> {
                })
                .setFormat(( r ) -> "BlaBla")
                .build();

        Record record3 = new SimpleRecord.RecordBuilder()
                .setTransactionID("BlaBla")
                .setAmount(new BigDecimal("156"))
                .setCurrency("USD")
                .setFormat(( r ) -> "BlaBla")
                .setValueDate("2020-2-20")
                .setValidator(( s ) -> {
                })
                .build();

        Assertions.assertEquals(record.hashCode(), record2.hashCode());
        Assertions.assertNotEquals(record.hashCode(), record3.hashCode());
    }

}
