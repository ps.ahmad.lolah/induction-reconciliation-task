package com.progressoft.jip8;

import com.progressoft.jip8.validators.Validators;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.function.Function;

public class SimpleRecord implements Record {

    private final BigDecimal amount;
    private final String currency;
    private final String valueDate;
    private final String transactionID;
    private Function<SimpleRecord, Object> format;
    private String foundIn;

    private SimpleRecord( RecordBuilder builder ) {
        this.transactionID = builder.transactionID;
        this.amount = builder.amount;
        this.currency = builder.currency;
        this.valueDate = builder.valueDate;
        this.format = builder.format;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }

    public String getValueDate() {
        return valueDate;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public String getKEY() {
        return transactionID;
    }

    @Override
    public boolean equals( Object o ) {
        if (this == o) return true;
        if (!(o instanceof Record)) return false;
        SimpleRecord SimpleRecord = (com.progressoft.jip8.SimpleRecord) o;
        return Objects.equals(transactionID, SimpleRecord.transactionID) &&
                amount.compareTo(SimpleRecord.amount) == 0 &&
                Objects.equals(currency, SimpleRecord.currency) &&
                Objects.equals(valueDate, SimpleRecord.valueDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(transactionID, amount, currency, valueDate);
    }


    public Object format() {
        return format.apply(this);
    }

    @Override
    public void setFoundIn( String source ) {
        this.foundIn = source;
    }

    @Override
    public String getFoundIn() {
        return foundIn;
    }

    public static class RecordBuilder {
        private Function<SimpleRecord, Object> format;
        private Validators<Object> validators;
        private String transactionID;
        private BigDecimal amount;
        private String currency;
        private String valueDate;

        public RecordBuilder setTransactionID( String KEY ) {
            this.transactionID = KEY;
            return this;
        }

        public RecordBuilder setFormat( Function<SimpleRecord, Object> format ) {
            this.format = format;
            return this;
        }

        public RecordBuilder setAmount( BigDecimal amount ) {
            this.amount = amount;
            return this;
        }

        public RecordBuilder setCurrency( String currency ) {
            this.currency = currency;
            return this;
        }

        public RecordBuilder setValueDate( String valueDate ) {
            this.valueDate = valueDate;
            return this;
        }

        public RecordBuilder setValidator( Validators<Object> validators ) {
            this.validators = validators;
            return this;
        }

        public SimpleRecord build() {
            SimpleRecord SimpleRecord = new SimpleRecord(this);
            validateRecordObject(SimpleRecord);
            return SimpleRecord;
        }

        private void validateRecordObject( SimpleRecord SimpleRecord ) {
            Objects.requireNonNull(SimpleRecord.amount, "null amount");
            Objects.requireNonNull(SimpleRecord.currency, "null currency");
            Objects.requireNonNull(SimpleRecord.transactionID, "null transaction id");
            Objects.requireNonNull(SimpleRecord.valueDate, "null date");
            Objects.requireNonNull(SimpleRecord.format, "null format");
            Objects.requireNonNull(this.validators, "null validators");

            validators.validate(SimpleRecord.format());
        }

    }
}
