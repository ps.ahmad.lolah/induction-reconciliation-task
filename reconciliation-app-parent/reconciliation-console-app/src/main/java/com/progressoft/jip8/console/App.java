package com.progressoft.jip8.console;

import com.progressof.jip8.FileResultHandler;
import com.progressoft.jip8.Reconciliator;
import com.progressoft.jip8.RecordsService;
import com.progressoft.jip8.ResultHandler;
import com.progressoft.jip8.ServiceFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class App {
    public static void main( String[] args ) throws IOException {
        Scanner scanner = new Scanner(System.in);
        RecordsService sourceService = prepareSource(scanner);

        RecordsService targetService = prepareTarget(scanner);

        String resultDir = ("/home/user/reconciliation-results");
        ResultHandler resultHandler = prepareResultViewer(resultDir);

        Reconciliator reconciliator = new Reconciliator(resultHandler, sourceService, targetService);
        reconciliator.reconcile();
        System.out.println("Reconciliation finished.\n" +
                "Result files are availble in directory " + resultDir);
    }

    private static ResultHandler prepareResultViewer( String resultDir ) throws IOException {
        Files.createDirectories(Paths.get(resultDir));
        Path matchPath = Paths.get(resultDir, "/matching-transactions.csv");
        Path misMatchPath = Paths.get(resultDir, "/mismatched-transactions.csv");
        Path missingPath = Paths.get(resultDir, "/missing-transactions.csv");

        return new FileResultHandler.FileResultViewerBuilder()
                .setMatchPath(matchPath)
                .setMisMatchPath(misMatchPath)
                .setMissingPath(missingPath)
                .build();

    }

    private static RecordsService prepareTarget( Scanner scanner ) throws IOException {
        Path targetPath;
        String targetFormat;

        System.out.println("Enter Target file Location: ");
        targetPath = Paths.get(scanner.nextLine());
        System.out.println("Enter Target File Format: ");
        targetFormat = scanner.nextLine();

        return ServiceFactory.newService(targetPath, targetFormat);
    }

    private static RecordsService prepareSource( Scanner scanner ) throws IOException {
        Path sourcePath;
        String sourceFormat;

        System.out.println("Enter Source File Location: ");
        sourcePath = Paths.get(scanner.nextLine());
        System.out.println("Enter Source File Format: ");
        sourceFormat = (scanner.nextLine());

        return ServiceFactory.newService(sourcePath, sourceFormat);
    }
}
