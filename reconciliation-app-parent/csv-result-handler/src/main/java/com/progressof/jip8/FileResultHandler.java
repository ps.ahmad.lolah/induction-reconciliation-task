package com.progressof.jip8;
import com.progressoft.jip8.Record;
import com.progressoft.jip8.ResultHandler;

import java.io.Closeable;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;

public class FileResultHandler implements ResultHandler, Closeable {
    private PrintWriter matchFile;
    private PrintWriter misMatchFile;
    private PrintWriter missingFile;

    FileResultHandler( FileResultViewerBuilder builder ) throws IOException {
        matchFile = new PrintWriter(Files.newOutputStream(builder.matchPath));
        misMatchFile = new PrintWriter(Files.newOutputStream(builder.misMatchPath));
        missingFile = new PrintWriter(Files.newOutputStream(builder.missingPath));

        writeFileHeaders();

    }


    private void writeFileHeaders() throws IOException {
        matchFile.write("transaction id,amount,currecny code,value date\n");
        misMatchFile.write("found in file,transaction id,amount,currecny code,value date\n");
        missingFile.write("found in file,transaction id,amount,currency code,value date\n");
    }


    @Override
    public void handleMatch(Record record ) {
        matchFile.write(record.format() + "\n");
        matchFile.flush();

    }

    @Override
    public void handleMisMatch(Record source, Record target ) {
        misMatchFile.write("SOURCE," + source.format() + "\n");
        misMatchFile.write("TARGET," + target.format() + "\n");
        misMatchFile.flush();
    }

    @Override
    public void handleMissing(Record record, String from ) {
        missingFile.write(from + "," + record.format() + "\n");
        missingFile.flush();
    }

    @Override
    public void close() throws IOException {
        matchFile.close();
        misMatchFile.close();
        missingFile.close();
    }

    public static class FileResultViewerBuilder {
        private Path matchPath;
        private Path misMatchPath;
        private Path missingPath;

        public FileResultViewerBuilder setMatchPath( Path matchPath ) {
            Objects.requireNonNull(matchPath, "null match file path");

            this.matchPath = (matchPath);
            return this;
        }

        public FileResultViewerBuilder setMisMatchPath( Path misMatchPath ) {
            Objects.requireNonNull(misMatchPath, "null mismatch file path");

            this.misMatchPath = (misMatchPath);
            return this;
        }

        public FileResultViewerBuilder setMissingPath( Path missingPath ) {
            Objects.requireNonNull(missingPath, "null missing file path");

            this.missingPath = (missingPath);
            return this;
        }

        public FileResultHandler build() throws IOException {
            FileResultHandler viewer = new FileResultHandler(this);
            return viewer;

        }
    }
}
