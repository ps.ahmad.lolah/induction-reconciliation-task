package com.progressoft.jip8;

import com.progressof.jip8.FileResultHandler;
import com.progressoft.jip8.csv.CSVReader;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileResultHandlerTest {
    Path path = Paths.get(".");

    @Test
    public void givenNullPath_whenConstruct_thenThrowNullPointerException() {
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class,
                () -> {
                    new FileResultHandler.FileResultViewerBuilder()
                            .setMatchPath(null)
                            .setMisMatchPath(path)
                            .setMissingPath(path)
                            .build();
                });
        Assertions.assertEquals("null match file path", exception.getMessage());

        exception = Assertions.assertThrows(NullPointerException.class,
                () -> new FileResultHandler.FileResultViewerBuilder()
                        .setMatchPath(path)
                        .setMisMatchPath(null)
                        .setMissingPath(path)
                        .build());
        Assertions.assertEquals("null mismatch file path", exception.getMessage());

        exception = Assertions.assertThrows(NullPointerException.class,
                () -> new FileResultHandler.FileResultViewerBuilder()
                        .setMatchPath(path)
                        .setMisMatchPath(path)
                        .setMissingPath(null)
                        .build());
        Assertions.assertEquals("null missing file path", exception.getMessage());
    }

    @Test
    public void givenValidPaths_whenViewMatch_thenWorkAsExpected() throws IOException {
        Path matchPath = Files.createTempFile("match", ".csv");
        Path misMatchPath = Files.createTempFile("mismatch", ".csv");
        Path missingPath = Files.createTempFile("missing", ".csv");

        FileResultHandler resultViewer = new FileResultHandler.FileResultViewerBuilder()
                .setMatchPath(matchPath)
                .setMisMatchPath(misMatchPath)
                .setMissingPath(missingPath)
                .build();

        Record record = new SimpleRecord.RecordBuilder()
                .setTransactionID("BlaBla")
                .setAmount(new BigDecimal("156"))
                .setCurrency("JOD")
                .setValueDate("2020-2-20")
                .setFormat(( r ) -> "BlaBla,156,JOD,2020-2-20")
                .setValidator(( s ) -> {
                })
                .build();

        resultViewer.handleMatch(record);
        resultViewer.handleMisMatch(record, record);
        resultViewer.handleMissing(record, "Source");
        resultViewer.close();

        CSVReader matchReader = new CSVReader(matchPath);
        CSVReader mismatchReader = new CSVReader(misMatchPath);
        CSVReader missingReader = new CSVReader(missingPath);

        Assertions.assertEquals(matchReader.get(), "transaction id,amount,currecny code,value date");
        Assertions.assertEquals(matchReader.get(), "BlaBla,156,JOD,2020-2-20");
        Assertions.assertEquals(matchReader.get(), null);


        Assertions.assertEquals(mismatchReader.get(), "found in file,transaction id,amount,currecny code,value date");
        Assertions.assertEquals(mismatchReader.get(), "SOURCE,BlaBla,156,JOD,2020-2-20");
        Assertions.assertEquals(mismatchReader.get(), "TARGET,BlaBla,156,JOD,2020-2-20");
        Assertions.assertEquals(mismatchReader.get(), null);

        Assertions.assertEquals(missingReader.get(), "found in file,transaction id,amount,currency code,value date");
        Assertions.assertEquals(missingReader.get(), "Source,BlaBla,156,JOD,2020-2-20");
        Assertions.assertEquals(missingReader.get(), null);
    }
}
