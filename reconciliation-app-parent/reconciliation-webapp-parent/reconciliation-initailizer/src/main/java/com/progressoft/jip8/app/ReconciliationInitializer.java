package com.progressoft.jip8.app;

import javax.servlet.ServletContainerInitializer;
import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;
import java.util.Set;

public class ReconciliationInitializer implements ServletContainerInitializer {
    @Override
    public void onStartup( Set<Class<?>> c, ServletContext ctx ) {

        prepareSignInServlet(ctx);

        prepareDownloadServlet(ctx);

        prepareResultServlet(ctx);

        prepareResultViewerServlet(ctx);
    }



    private void prepareResultServlet( ServletContext ctx ) {
        ReconciliationServlet reconciliationServlet = new ReconciliationServlet(new FileHolder(),new ListResultHandler());
        ServletRegistration.Dynamic reconciliationServlet1 = ctx.addServlet("reconciliationServlet", reconciliationServlet);
        reconciliationServlet1.addMapping("/result");
    }

    private void prepareResultViewerServlet( ServletContext ctx ) {
        ResultTableAPI resultTableAPI = new ResultTableAPI();
        ServletRegistration.Dynamic resultServlet = ctx.addServlet("resultViewer", resultTableAPI);
        resultServlet.addMapping("/api/result_viewer");
    }


    private void prepareDownloadServlet( ServletContext ctx)  {
        DownloadServlet reconciliationServlet = new DownloadServlet();
        ServletRegistration.Dynamic reconciliationServlet1 = ctx.addServlet("DownloadServlet", reconciliationServlet);
        reconciliationServlet1.addMapping("/api/download");
    }

    private void prepareSignInServlet( ServletContext ctx ) {
        SignInAuthentication signInAuthentication = new StaticSignInAuthentication();
        SignInServlet signInServlet = new SignInServlet(signInAuthentication);
        ServletRegistration.Dynamic loginServlet = ctx.addServlet("signInServlet", signInServlet);
        loginServlet.addMapping("/sign_in");
    }


}
