package com.progressoft.jip8.app;

public interface SignInAuthentication {
    boolean isAuthorized(String userName,String password);
}
