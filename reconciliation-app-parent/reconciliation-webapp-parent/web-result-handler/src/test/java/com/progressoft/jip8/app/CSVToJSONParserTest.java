package com.progressoft.jip8.app;

import com.progressoft.jip8.Record;
import com.progressoft.jip8.SimpleRecord;
import com.progressoft.jip8.csv.CSVReader;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CSVToJSONParserTest {
    @Test
    public void givenNullPath_whenConvert_thenThrowNullPointerException() throws IOException {
        Path tempFile = Files.createTempFile("test", "csv");
        Assertions.assertThrows(NullPointerException.class, () -> CSVToJSONParser.getJSONDir(null, tempFile, tempFile));
        Assertions.assertThrows(NullPointerException.class, () -> CSVToJSONParser.getJSONDir(tempFile, null, tempFile));
        Assertions.assertThrows(NullPointerException.class, () -> CSVToJSONParser.getJSONDir(tempFile,tempFile, null));
    }

    @Test
    public void givenValidPaths_whenViewMatch_thenWorkAsExpected() throws IOException {

        FileHolder fileHolder = new FileHolder();
        CSVResultHandler resultViewer = new CSVResultHandler(fileHolder);
        Path matchPath = Paths.get(fileHolder.getCSVDir().toAbsolutePath().toString(), "/matching-transactions.csv");
        Path misMatchPath = Paths.get(fileHolder.getCSVDir().toAbsolutePath().toString(), "/mismatched-transactions.csv");
        Path missingPath = Paths.get(fileHolder.getCSVDir().toAbsolutePath().toString(), "/missing-transactions.csv");

        Record record = new SimpleRecord.RecordBuilder()
                .setTransactionID("BlaBla")
                .setAmount(new BigDecimal("156"))
                .setCurrency("JOD")
                .setValueDate("2020-2-20")
                .setFormat(( r ) -> "BlaBla,156,JOD,2020-2-20")
                .setValidator(( s ) -> {
                })
                .build();

        resultViewer.handleMatch(record);
        resultViewer.handleMisMatch(record, record);
        resultViewer.handleMissing(record, "Source");
        resultViewer.close();


        CSVReader matchReader = new CSVReader(matchPath);
        CSVReader mismatchReader = new CSVReader(misMatchPath);
        CSVReader missingReader = new CSVReader(missingPath);

        Assertions.assertEquals(matchReader.get(), "#,transaction id,amount,currecny code,value date");
        Assertions.assertEquals(matchReader.get(), "1,BlaBla,156,JOD,2020-2-20");
        Assertions.assertEquals(matchReader.get(), null);


        Assertions.assertEquals(mismatchReader.get(), "#,found in file,transaction id,amount,currecny code,value date");
        Assertions.assertEquals(mismatchReader.get(), "1,SOURCE,BlaBla,156,JOD,2020-2-20");
        Assertions.assertEquals(mismatchReader.get(), "2,TARGET,BlaBla,156,JOD,2020-2-20");
        Assertions.assertEquals(mismatchReader.get(), null);

        Assertions.assertEquals(missingReader.get(), "#,found in file,transaction id,amount,currency code,value date");
        Assertions.assertEquals(missingReader.get(), "1,Source,BlaBla,156,JOD,2020-2-20");
        Assertions.assertEquals(missingReader.get(), null);


        Path jsonDir = fileHolder.getJSONDir();
        Assertions.assertNotNull(jsonDir);
        String[] list = jsonDir.toFile().list();
        Assertions.assertEquals(3, list.length);
        for (String file : list) {
            Assertions.assertTrue(file.endsWith("json"));
        }


    }

}
