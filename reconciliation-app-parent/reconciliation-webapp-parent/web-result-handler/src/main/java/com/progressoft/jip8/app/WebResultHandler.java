package com.progressoft.jip8.app;

import com.progressoft.jip8.Record;
import com.progressoft.jip8.ResultHandler;

import java.io.IOException;

public class WebResultHandler implements ResultHandler {
    private ListResultHandler listResultHandler;
    private CSVResultHandler csvResultHandler;

    WebResultHandler( FileHolder fileHolder, ListResultHandler listResultHandler ) throws IOException {
        csvResultHandler = new CSVResultHandler(fileHolder);
        this.listResultHandler = listResultHandler;

    }
    @Override
    public void handleMatch(Record record ) {
        listResultHandler.handleMatch(record);
        csvResultHandler.handleMatch(record);
    }

    @Override
    public void handleMisMatch(Record source, Record target ) {
        source.setFoundIn("SOURCE");
        target.setFoundIn("TARGET");
        listResultHandler.handleMisMatch(source, target);
        csvResultHandler.handleMisMatch(source, target);
    }

    @Override
    public void handleMissing(Record record, String from ) {
        record.setFoundIn(from.replace(",",""));
        listResultHandler.handleMissing(record, from);
        csvResultHandler.handleMissing(record, from);
    }

    @Override
    public void close() throws IOException {
        csvResultHandler.close();
    }

}
