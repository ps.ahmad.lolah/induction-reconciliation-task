package com.progressoft.jip8.app;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.progressoft.jip8.validators.FileValidator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

public class CSVToJSONParser {

    public static Path getJSONDir(Path missingPath, Path matchPath, Path misMatchPath) throws IOException {
        validatePaths(missingPath, matchPath, misMatchPath);

        return convertCSVtoJSON(missingPath, matchPath, misMatchPath);
    }


    private static Path convertCSVtoJSON(Path matchPath, Path missingPath, Path misMatchPath) throws IOException {
        List<Map<?, ?>> matchList = getJsonList(matchPath);
        List<Map<?, ?>> missingList = getJsonList(missingPath);
        List<Map<?, ?>> misMatchList = getJsonList(misMatchPath);


        Path JSONDir = Files.createTempDirectory("JSON");
        Files.createDirectories(JSONDir);
        String dir = JSONDir.toAbsolutePath().toString();
        writeAsJSON(matchList, missingList, misMatchList, dir);
        return JSONDir;

    }

    private static void validatePaths(Path missingPath, Path matchPath, Path misMatchPath) throws FileNotFoundException {
        Validate(matchPath);
        Validate(missingPath);
        Validate(misMatchPath);

    }

    private static void Validate(Path path) throws FileNotFoundException {

        FileValidator.isValid(path, "csv");
    }

    private static void writeAsJSON(List<Map<?, ?>> matchList, List<Map<?, ?>> missingList, List<Map<?, ?>> misMatchList, String dir) throws IOException {
        Path JsonMatchPath = Paths.get(dir, "/matching-transactions.json");
        Path JsonMissingPath = Paths.get(dir, "/missing-transactions.json");
        Path JsonMisMatchPath = Paths.get(dir, "/misMatch-transactions.json");

        writeListToJsonFile(matchList, JsonMatchPath);
        writeListToJsonFile(missingList, JsonMissingPath);
        writeListToJsonFile(misMatchList, JsonMisMatchPath);
    }


    private static List<Map<?, ?>> getJsonList(Path matchPath) {
        File matchFile = matchPath.toFile();
        return parseCSVtoJSON(matchFile);
    }

    private static void writeListToJsonFile(List<Map<?, ?>> file, Path path) throws IOException {

        PrintWriter printWriter = new PrintWriter(Files.newOutputStream(path));

        printWriter.write(file.toString());
        printWriter.flush();
    }

    private static List<Map<?, ?>> parseCSVtoJSON(File input) {
        CsvSchema csv = CsvSchema.emptySchema().withHeader();
        CsvMapper csvMapper = new CsvMapper();
        ObjectReader reader = csvMapper.reader();
        ObjectReader objectReader = reader.forType(Map.class);
        ObjectReader with = objectReader.with(csv);

        try {
            MappingIterator<Map<?, ?>> mappingIterator = with.readValues(input);
            return mappingIterator.readAll();
        } catch (Exception e) {
            throw new IllegalStateException("failed to parse csv to json: " + input.getName());
        }
    }
}
