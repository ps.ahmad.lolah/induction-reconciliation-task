package com.progressoft.jip8.app;

import com.progressoft.jip8.Record;
import com.progressoft.jip8.SimpleRecord;

import java.util.*;

public class ListResultHandler {
    private List<SimpleRecord> matchList;
    private List<SimpleRecord> misMatchList;
    private List<SimpleRecord> missingList;

    public ListResultHandler() {
        this.matchList = new ArrayList<>();
        this.misMatchList = new ArrayList<>();
        this.missingList = new ArrayList<>();
    }

    public Map<String, List<SimpleRecord>> getResultList() {
        Map<String, List<SimpleRecord>> resultList = new HashMap<>();
        resultList.put("match", matchList);
        resultList.put("missing", missingList);
        resultList.put("misMatch", misMatchList);
        return resultList;
    }

    public void handleMatch(Record record ) {
        Objects.requireNonNull(record, "null record");
        matchList.add((SimpleRecord) record);
    }


    public void handleMissing(Record record, String from ) {
        Objects.requireNonNull(record, "null record");
        missingList.add((SimpleRecord) record);
    }


    public void handleMisMatch(Record source, Record target ) {
        Objects.requireNonNull(source, "null source");
        Objects.requireNonNull(target, "null target");
        misMatchList.add((SimpleRecord) source);
        misMatchList.add((SimpleRecord) target);
    }

    public void clear() {
        matchList.clear();
        missingList.clear();
        misMatchList.clear();
    }
}
