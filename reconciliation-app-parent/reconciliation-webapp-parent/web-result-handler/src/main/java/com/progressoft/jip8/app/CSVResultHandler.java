package com.progressoft.jip8.app;

import com.progressoft.jip8.Record;
import com.progressoft.jip8.ResultHandler;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.util.Objects;

public class CSVResultHandler implements ResultHandler {
    private PrintWriter CSVMatchWriter;
    private PrintWriter CSVMisMatchFile;
    private PrintWriter CSVMissingFile;
    private int matchingCount;
    private int misMatchingCount;
    private int missingCount;
    private FileHolder fileHolder;

    public CSVResultHandler( FileHolder fileHolder ) throws IOException {
        Objects.requireNonNull(fileHolder,"null file holder");
        this.fileHolder = fileHolder;
        CSVMatchWriter = new PrintWriter(Files.newOutputStream(fileHolder.getMatchPath()));
        CSVMisMatchFile = new PrintWriter(Files.newOutputStream(fileHolder.getMisMatchPath()));
        CSVMissingFile = new PrintWriter(Files.newOutputStream(fileHolder.getMissingPath()));
        writeFileHeaders();

    }

    private void writeFileHeaders() throws IOException {
        CSVMatchWriter.write("#,transaction id,amount,currecny code,value date\n");
        CSVMisMatchFile.write("#,found in file,transaction id,amount,currecny code,value date\n");
        CSVMissingFile.write("#,found in file,transaction id,amount,currency code,value date\n");
    }

    @Override
    public void handleMatch(Record record ) {
        Objects.requireNonNull(record, "null record");
        CSVMatchWriter.write("" + (++matchingCount) + "," + record.format() + "\n");
        CSVMatchWriter.flush();

    }

    @Override
    public void handleMissing(Record record, String from ) {
        Objects.requireNonNull(record, "null record");
        String ressult = "" + ++missingCount + "," + from + "," + record.format() + "\n";
        CSVMissingFile.write(ressult.replace(",,",","));
        CSVMissingFile.flush();
    }

    @Override
    public void handleMisMatch(Record source, Record target ) {
        Objects.requireNonNull(source, "null source");
        Objects.requireNonNull(target, "null target");
        CSVMisMatchFile.write("" + ++misMatchingCount + ",SOURCE," + source.format() + "\n");
        CSVMisMatchFile.write("" + ++misMatchingCount + ",TARGET," + target.format() + "\n");
        CSVMisMatchFile.flush();
    }

    @Override
    public void close() throws IOException {
        CSVMatchWriter.close();
        CSVMisMatchFile.close();
        CSVMissingFile.close();
    }
}
