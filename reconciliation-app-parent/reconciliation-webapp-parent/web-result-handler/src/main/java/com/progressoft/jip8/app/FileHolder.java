package com.progressoft.jip8.app;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

public class FileHolder {
    private Path matchPath;
    private Path misMatchPath;
    private Path missingPath;
    private Path CSVDir;


    public FileHolder( ) {
        this.CSVDir = createTempCSVDir();
        matchPath = Paths.get(CSVDir.toAbsolutePath().toString(), "/matching-transactions.csv");
        misMatchPath = Paths.get(CSVDir.toAbsolutePath().toString(), "/mismatched-transactions.csv");
        missingPath = Paths.get(CSVDir.toAbsolutePath().toString(), "/missing-transactions.csv");
    }

    private Path createTempCSVDir() {
        Path csv;
        try {
            csv = Files.createTempDirectory("CSV");
        } catch (IOException e) {
            throw new IllegalStateException("failed to create temp file.");
        }
        return csv;
    }

    public Path getMatchPath() {
        return matchPath;
    }

    public Path getMisMatchPath() {
        return misMatchPath;
    }

    public Path getMissingPath() {
        return missingPath;
    }

    public Path getCSVDir() {
        return CSVDir;
    }

    public Path getJSONDir() throws IOException {
        return CSVToJSONParser.getJSONDir(matchPath, misMatchPath, missingPath);
    }

    public void clear() {
        String[] list = CSVDir.toFile().list();
        Objects.requireNonNull(list, "the csv directory is empty");
        for (String path : list) {
            Paths.get(path).toFile().delete();
        }
    }
}
