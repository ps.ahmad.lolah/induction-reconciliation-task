<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Internal Server Error</title>
    <!--pageMeta-->
    <!-- Loading Bootstrap -->
    <link href="${pageContext.request.contextPath}/css/bootstrap.css" rel="stylesheet" />
    <!-- Loading Elements Styles -->
    <link href="${pageContext.request.contextPath}/errorPages/style.css" rel="stylesheet" />
    <!-- Loading Font Styles -->
    <link href="${pageContext.request.contextPath}/css/iconfont-style.css" rel="stylesheet" />
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
    <script src="https://cdn.bootcss.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--headerIncludes-->
</head>
<body data-spy="scroll" data-target=".navMenuCollapse">
<div id="wrap">
    <!-- 404 ERROR BLOCK -->
    <section id="error-500" class="cover-bg no-sep screen-height" style="background-image:url(images/bg51.jpg)">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="mega-title">5<i class="icon center-icon icon-compass2"></i><i class="icon center-icon icon-compass2"></i></h2>
                    <h2><strong>OOOOOOOOPS</strong></h2>
                    <h2 class="sep-bottom">Unfortunately,the website had a bad day.</h2>
                    <a href="${pageContext.request.contextPath}/reconcile" class="btn btn-lg btn-primary"><i class="icon icon-arrow-left"></i> Go back home</a>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /#wrap -->
</body>
</html>