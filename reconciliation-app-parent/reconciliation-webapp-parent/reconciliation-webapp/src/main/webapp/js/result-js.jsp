<%@page contentType="text/javascript"%>

    $("#match").on("click", function () {
        $(this).val("matched");
        $("#unmatched").val("");
        $("#missing").val("");
    });


    $("#unmatched").on("click", function () {
        $(this).val("unmatched");
        $("#match").val("");
        $("#missing").val("");
    });


    $("#missing").on("click", function () {
        $(this).val("missing");
        $("#match").val("");
        $("#unmatched").val("");
    });


    $("#match").on("click", function () {
        $.ajax({
            url: "${pageContext.request.contextPath}/api/result_viewer?file=match",
            type: "GET",
            contentType: "application/json",
            success: function (result) {
                var headers = "<tr><th>#</th>" + "<th>Transaction ID</th>" + "<th>Amount</th>" + "<th>Currency</th>" + "<th>Value date</th></tr>"
                $("#nav-match thead").html(headers)
                $.each(result, function (i, transaction) {
                    var selector = $("#nav-match tbody");
                    if (i === 0) {
                        selector.html("<tr> " + "<td>" + (i + 1) + "</td>" + "<td>" + transaction.transactionID + "</td>"
                            + "<td>" + transaction.amount + "</td>" + "<td>" + transaction.currency + "</td>" + "<td>" + transaction.valueDate + "</td>" + "</tr>")
                    } else
                        selector.append("<tr> " + "<td>" + (i + 1) + "</td>" + "<td>" + transaction.transactionID + "</td>"
                            + "<td>" + transaction.amount + "</td>" + "<td>" + transaction.currency + "</td>" + "<td>" + transaction.valueDate + "</td>" + "</tr>")
                })
            }
        });
    });


    $("#unmatched").on("click", function () {
        $.ajax({
            url: "${pageContext.request.contextPath}/api/result_viewer?file=misMatch",
            type: "GET",
            contentType: "application/json",
            success: function (result) {

                var headers = "<tr><th>#</th>" + "<th>" + "Found In" + "</th>" + "<th>Transaction ID</th>" + "<th>Amount</th>" + "<th>Currency</th>" + "<th>Value date</th></tr>"
                $("#nav-unMatch thead").html(headers)
                $.each(result, function (i, transaction) {
                    var selector = $("#nav-unMatch tbody");
                    if (i === 0) {
                        selector.html("<tr> " + "<td>" + (i + 1) + "</td>" + "<td>" + transaction.foundIn + "</td>" + "<td>" + transaction.transactionID+ "</td>"
                            + "<td>" + transaction.amount + "</td>" + "<td>" + transaction.currency + "</td>" + "<td>" + transaction.valueDate + "</td>" + "</tr>")
                    } else
                        selector.append("<tr> " + "<td>" + (i + 1) + "</td>" + "<td>" + transaction.foundIn + "</td>" + "<td>" + transaction.transactionID + "</td>"
                            + "<td>" + transaction.amount + "</td>" + "<td>" + transaction.currency + "</td>" + "<td>" + transaction.valueDate + "</td>" + "</tr>")
                })
            }
        });
    });


    $("#missing").on("click", function () {
        $.ajax({
            url: "${pageContext.request.contextPath}/api/result_viewer?file=missing",
            type: "GET",
            contentType: "application/json",
            success: function (result) {

                var headers = "<tr><th>#</th>" + "<th>" + "found in" + "</th>" + "<th>Transaction ID</th>" + "<th>Amount</th>" + "<th>Currency</th>" + "<th>Value date</th></tr>"
                $("#nav-contact thead").html(headers)
                $.each(result, function (i, transaction) {
                    var selector = $("#nav-contact tbody");
                    if (i === 0) {
                        selector.html("<tr> " + "<td>" + (i + 1) + "</td>" + "<td>" + transaction.foundIn + "</td>" + "<td>" + transaction.transactionID + "</td>"
                            + "<td>" + transaction.amount + "</td>" + "<td>" + transaction.currency + "</td>" + "<td>" + transaction.valueDate + "</td>" + "</tr>")
                    } else
                        selector.append("<tr> " + "<td>" + (i + 1) + "</td>" + "<td>" + transaction.foundIn + "</td>" + "<td>" + transaction.transactionID + "</td>"
                            + "<td>" + transaction.amount + "</td>" + "<td>" + transaction.currency + "</td>" + "<td>" + transaction.valueDate + "</td>" + "</tr>")
                })
            }
        });
    });

    var matchedHref;
    $(document).ready(function () {
        $.ajax({
            url: "${pageContext.request.contextPath}/api/result_viewer?file=match",
            type: "GET",
            contentType: "application/json",
            success: function (result) {
                $("#match").val("matched");
                $("#unmatched").val("");
                $("#missing").val("");
                var headers = "<tr><th>#</th>" + "<th>Transaction ID</th>" + "<th>Amount</th>" + "<th>Currency</th>" + "<th>Value date</th></tr>";

                $("#nav-match thead").html(headers);

                $.each(result, function (i, transaction) {
                    var selector = $("#nav-match tbody");
                    if (i === 0) {
                        selector.html("<tr> " + "<td>" + (i + 1) + "</td>" + "<td>" + transaction.transactionID + "</td>"
                            + "<td>" + transaction.amount + "</td>" + "<td>" + transaction.currency + "</td>" + "<td>" + transaction.valueDate + "</td>" + "</tr>")
                    } else
                        selector.append("<tr> " + "<td>" + (i + 1) + "</td>" + "<td>" + transaction.transactionID + "</td>"
                            + "<td>" + transaction.amount + "</td>" + "<td>" + transaction.currency + "</td>" + "<td>" + transaction.valueDate + "</td>" + "</tr>")
                })
            }
        });
    });
