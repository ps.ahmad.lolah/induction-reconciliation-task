<%@taglib prefix="jip" tagdir="/WEB-INF/tags" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Result</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/docs/assets/css/bootstrap.min.css">
    <script src="${pageContext.request.contextPath}/jquery/jquery-3.4.1.js"></script>
    <style>
    </style>
</head>
<body style="background-image: url('http://getwallpapers.com/wallpaper/full/a/5/d/544750.jpg');">

<section id="tabs" class="project-tab">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <nav>
                    <div class="nav nav-tabs nav-fill" id="nav-tab"role="tablist">
                        <a class="nav-item nav-link active" id="match" style="color: #ffc107"  data-toggle="tab" href="#nav-match"
                           role="tab" aria-controls="nav-match"  aria-selected="true">Match</a>
                        <a class="nav-item nav-link" id="unmatched" style="color: #ffc107"  data-toggle="tab" href="#nav-unMatch"
                           role="tab" aria-controls="nav-unMatch" aria-selected="false">Mismatch</a>
                        <a class="nav-item nav-link" id="missing"  style="color: #ffc107"  data-toggle="tab" href="#nav-contact"
                           role="tab" aria-controls="nav-contact" aria-selected="false">Missing</a>
                    </div>
                </nav>


                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-match" role="tabpanel" aria-labelledby="match">
                        <jip:table />
                    </div>
                    <div class="tab-pane fade" id="nav-unMatch" role="tabpanel" aria-labelledby="unmatched">
                        <jip:table />
                    </div>
                    <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="missing">
                        <jip:table />
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<form action="${pageContext.request.contextPath}/reconcile" method="post">
    <div class="form-group">
        <input type="submit" style="background: #ffc107;color: #000000" onclick="nextPrev(-2)" id="goBack" value="Compare New File"
               class="btn btn-primary mr-4 float-right">
    </div>
    <a class="btn btn-primary dropdown-toggle mr-4 float-right"  style="background: #ffc107;color: #000000" type="button" data-toggle="dropdown" aria-haspopup="true"
       aria-expanded="false">Download</a>

    <div class="dropdown-menu">
        <a class="dropdown-item" href="${pageContext.request.contextPath}/api/download?format=csv" id="csvbtn">CSV</a>
        <a class="dropdown-item" href="${pageContext.request.contextPath}/api/download?format=json" id="jsonbtn">JSON</a>
    </div>
</form>

<script src="${pageContext.request.contextPath}/js/result-js.jsp"></script>



<script src="${pageContext.request.contextPath}/popper/popper.min.js"></script>
<script src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.min.js"></script>


</body>


</html>
