<%@taglib prefix="jip" tagdir="/WEB-INF/tags" %>

<html>
<head>

    <title>Reconcile</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/wizardAssests/wizardCSS.css">
    <link href="${pageContext.request.contextPath}/docs/assets/css/bootstrap.min.css" rel="stylesheet">

    <script src="${pageContext.request.contextPath}/jquery/jquery-3.4.1.js"></script>
    <script src="${pageContext.request.contextPath}/popper/popper.min.js"></script>

</head>
<body>
<form action="${pageContext.request.contextPath}/result" id="regForm" method="post" enctype="multipart/form-data">
    <h1 style="color: #FFFFFF">Reconciliation</h1>

    <div class="tab">Source File
        <jip:section fileName="sourceFileName" format="sourceFileFormat" file="sourceFile"/>
    </div>
    <div class="tab">Target File:
        <jip:section fileName="targetFileName" format="targetFileFormat" file="targetFile"/>
    </div>
    <div class="tab"><h3>Confirm:</h3>
        <div class="row">
            <div class="column">
                <jip:card cardHeader="Source File" name="Source Name:" format="Source Format:" nameCardID="sourceNameCard"
                          sourceCardID="sourceFormatCard"/>
            </div>
            <div class="column">
                <jip:card cardHeader="Target File" name="Target Name:" format="Target Format:" nameCardID="targetNameCard"
                          sourceCardID="targetFormatCard"/>
            </div>
        </div>
    </div>
    <br/>
    <div style="overflow:auto;">
        <div style="float:left;">
            <div class="form-group">
                <input type="button" onclick="nextPrev(-1)" id="prevBtn" value="Previous" class="btn wizard_btns">
            </div>
        </div>
        <div style="float:right;">
            <div class="form-group">
                <input type="button" onclick="nextPrev(1)" id="nextBtn" value="Next" class="btn wizard_btns">
            </div>
        </div>
        &nbsp;
        <div style="float:right;">
            <div class="form-group">
                <input type="button" onclick="nextPrev(-2)" id="cancelBtn" value="Cancel" style="display:none"
                       class="btn wizard_btns">
            </div>
        </div>
        &nbsp;
    </div>
    <!-- Circles which indicates the steps of the form: -->
    <div style="text-align:center;margin-top:40px;">
        <span class="step"></span>
        <span class="step"></span>
        <span class="step"></span>
    </div>
</form>


<script>
    $(document).ready(function () {
        var skip = "${sessionScope.reconcileFailed}";
        if (skip != "") {
            alert(skip);
        }
    });
</script>
<script src="${pageContext.request.contextPath}/wizardAssests/wizardJavaScript.js"></script>
</body>
</html>
