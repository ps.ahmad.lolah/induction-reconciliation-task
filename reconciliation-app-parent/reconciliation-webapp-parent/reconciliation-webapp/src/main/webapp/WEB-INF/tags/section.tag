<%@attribute name="fileName" rtexprvalue="true" required="true" type="java.lang.String" %>
<%@attribute name="format" rtexprvalue="true" required="true" type="java.lang.String" %>
<%@attribute name="file" rtexprvalue="true" required="true" type="java.lang.String" %>


<label for="${fileName}"></label><input type="text" class="form-control"
                                        name="${fileName}" id="${fileName}" required>
<br/>
<label for="${format}">Format</label>
<select name="${format}" class="form-control"  id="${format}" required>
    <option value="csv">CSV</option>
    <option value="json">JSON</option>
</select>
<br/>
<br/>
<input type="file" class="form-control" id="${file}"  name="${file}" required>
