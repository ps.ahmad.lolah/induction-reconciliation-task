<%@attribute name="name" rtexprvalue="true" required="true" type="java.lang.String" %>
<%@attribute name="format" rtexprvalue="true" required="true" type="java.lang.String" %>
<%@attribute name="nameCardID" rtexprvalue="true" required="true" type="java.lang.String" %>
<%@attribute name="sourceCardID" rtexprvalue="true" required="true" type="java.lang.String" %>
<%@attribute name="cardHeader" rtexprvalue="true" required="true" type="java.lang.String" %>


<div class="card bg-light mb-5" style="max-width: 18rem;">
    <div class="card-header" style="color: black; font-weight:bold;">${cardHeader}</div>
    <div class="card-body">
        <p class="card-text" >
        <p style="color: black; font-weight:bold;">
            ${name}<br/> <span id="${nameCardID}"></span>
        </p>
        <p style="color: black; font-weight:bold;">
            ${format}<br/> <span id="${sourceCardID}"></span>
        </p>
        </p>
    </div>
</div>