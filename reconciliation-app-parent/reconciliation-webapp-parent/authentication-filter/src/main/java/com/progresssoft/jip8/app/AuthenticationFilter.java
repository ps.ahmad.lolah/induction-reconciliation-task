package com.progresssoft.jip8.app;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class AuthenticationFilter implements Filter {

    private ServletContext context;

    @Override
    public void init( FilterConfig fConfig ) throws ServletException {
        this.context = fConfig.getServletContext();
        this.context.log("AuthenticationFilter initialized");
    }

    @Override
    public void doFilter( ServletRequest request, ServletResponse response, FilterChain chain ) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;

        HttpSession session = req.getSession(false);
        if(session == null)
            res.sendRedirect("/sign_in");


        Object email = session.getAttribute("email");
        if (email == null) {
            this.context.log("Unauthorized access request");
            res.sendRedirect("/sign_in");
        } else {
            chain.doFilter(request, response);
        }
    }


    @Override
    public void destroy() {

    }
}
