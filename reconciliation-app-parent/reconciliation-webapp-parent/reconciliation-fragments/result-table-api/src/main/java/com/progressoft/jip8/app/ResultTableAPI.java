package com.progressoft.jip8.app;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.progressoft.jip8.SimpleRecord;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class ResultTableAPI extends HttpServlet {
    private Gson gson;

    @Override
    public void init() throws ServletException {
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        gson = builder.create();
    }

    @Override
    protected void doGet( HttpServletRequest req, HttpServletResponse resp ) throws ServletException, IOException {
        HttpSession session = req.getSession();
        // TODO this is useless in ajax case
        String step = (String) session.getAttribute("step");
        if (!step.equals("result")) {
            resp.sendRedirect("/reconcile");
            return;
        }


        Map<String, List<SimpleRecord>> resultList = (Map<String, List<SimpleRecord>>) session.getAttribute("resultList");

        List<SimpleRecord> match = resultList.get(req.getParameter("file"));
        resp.setContentType("application/json");
        gson.toJson(match, resp.getWriter());
    }
}


