package com.progressoft.jip8.app;

import com.progressoft.jip8.Reconciliator;
import com.progressoft.jip8.RecordsService;
import com.progressoft.jip8.ServiceFactory;
import com.progressoft.jip8.SimpleRecord;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

public class ReconciliationServlet extends HttpServlet {
    private FileHolder fileHolder;
    private ListResultHandler listResultHandler;

    public ReconciliationServlet( FileHolder fileHolder, ListResultHandler listResultHandler ) {
        this.fileHolder = fileHolder;
        this.listResultHandler = listResultHandler;
    }

    @Override
    protected void doGet( HttpServletRequest req, HttpServletResponse resp ) throws ServletException, IOException {
        resp.sendRedirect("/reconcile");
    }

    @Override
    protected void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String step = (String) session.getAttribute("step");
        if (!step.equals("reconcile")) {
            response.sendRedirect("/reconcile");
            return;
        }


        if (!ServletFileUpload.isMultipartContent(request)) {
            dispatchToReconcilePage(request, response, "something went wrong.");
            return;
        }

        try {
            session.setAttribute("step", "result");
            fileHolder.clear();
            listResultHandler.clear();

            readAndReconcile(request, response);

            Map<String, List<SimpleRecord>> resultList = listResultHandler.getResultList();
            session.setAttribute("resultList", resultList);

            putResultPathsInSession(session);
        } catch (Exception ex) {
            dispatchToReconcilePage(request, response, ex.getMessage());
            return;
        }
        request.getRequestDispatcher("/WEB-INF/views/result.jsp").forward(request, response);

    }

    private void putResultPathsInSession(HttpSession session ) throws IOException {
        session.setAttribute("csv", fileHolder.getCSVDir().normalize().toString());
        session.setAttribute("json", fileHolder.getJSONDir().normalize().toString());
    }

    private void readAndReconcile( HttpServletRequest request, HttpServletResponse response ) throws Exception {
        List<FileItem> multiparts = new ServletFileUpload(
                new DiskFileItemFactory()).parseRequest(request);
        HttpSession session = request.getSession();
        session.removeAttribute("reconcileFailed");

        // TODO enhance by using below
//        Part part = request.getPart("source");
        RecordsService sourceService = getService(multiparts, 0, 1, 2);
        RecordsService targetService = getService(multiparts, 3, 4, 5);

        // TODO create fileHolder and listResultHandler as local variables
        WebResultHandler webResultHandler = new WebResultHandler(fileHolder, listResultHandler);
        Reconciliator reconciliator = new Reconciliator(webResultHandler, sourceService, targetService);
        reconciliator.reconcile();


    }

    private void dispatchToReconcilePage( HttpServletRequest request, HttpServletResponse response, String message ) throws ServletException, IOException {
        HttpSession session = request.getSession();
        session.setAttribute("reconcileFailed", message);
        request.getRequestDispatcher("/reconcile").forward(request, response);
    }


    private RecordsService getService( List<FileItem> multiparts, int fileNameIndex, int fileFormatIndex, int fileIndex ) throws Exception {
        FileItem fileItem = multiparts.get(fileNameIndex);
        String FileName = fileItem.getString();
        String FileFormat = multiparts.get(fileFormatIndex).getString();
        FileItem FileItem = multiparts.get(fileIndex);
        File TempFile = newTempFile(FileName, FileItem);

        return ServiceFactory.newService(Paths.get(TempFile.getPath()), FileFormat);
    }

    private File newTempFile( String name, FileItem file ) throws Exception {
        File tempFile = null;

        if (!file.isFormField()) {
            String format = file.getContentType().split("/")[1];

            tempFile = File.createTempFile(name, "." + format);
            file.write(tempFile);
        }
        return tempFile;
    }

}
