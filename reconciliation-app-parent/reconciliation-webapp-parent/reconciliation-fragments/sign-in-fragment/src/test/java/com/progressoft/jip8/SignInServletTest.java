package com.progressoft.jip8;

import com.progressoft.jip8.app.SignInServlet;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

import static org.mockito.Mockito.when;

public class SignInServletTest {
    @Mock
    HttpServletRequest request;
    @Mock
    HttpServletResponse response;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void givenDispatcher_whenDoGet_thenRunAsExpected() throws IOException, ServletException {
        HttpSession httpSession = getProxyHttpSession();
        when(request.getRequestDispatcher("/index.jsp")).thenReturn(new RequestDispatcherMock());
        when(request.getSession()).thenReturn(httpSession);
        SignInServlet signInServlet = new SignInServlet(( e, p ) -> true);
        Assertions.assertDoesNotThrow(() -> signInServlet.doGet(request, response));
    }

    @Test
    public void givenValidUserNameAndPassword_whenDoPost_thenRunAsExpected() throws IOException, ServletException {
        HttpSession httpSession = getProxyHttpSession();

        when(request.getRequestDispatcher("/reconcile")).thenReturn(new RequestDispatcherMock());
        when(request.getParameter("password")).thenReturn("test");
        when(request.getParameter("email")).thenReturn("test@test.com");
        when(request.getSession()).thenReturn(httpSession);

        SignInServlet signInServlet = new SignInServlet(( e, p ) -> true);
        Assertions.assertDoesNotThrow(() -> signInServlet.doPost(request, response));
    }

    @Test
    public void givenNullUserNameAndPassword_whenDoPost_thenRunAsExpected() throws IOException, ServletException {
        when(request.getRequestDispatcher("/index.jsp")).thenReturn(new RequestDispatcherMock());
        when(request.getParameter("password")).thenReturn(null);
        when(request.getParameter("email")).thenReturn(null);
        HttpSession httpSession = getProxyHttpSession();
        when(request.getSession()).thenReturn(httpSession);

        SignInServlet signInServlet = new SignInServlet(( e, p ) -> true);
        Assertions.assertDoesNotThrow(() -> signInServlet.doPost(request, response));
    }


    @Test
    public void givenInValidUserNameAndPassword_whenDoPost_thenRunAsExpected() throws IOException, ServletException {
        when(request.getRequestDispatcher("/index.jsp")).thenReturn(new RequestDispatcherMock());
        when(request.getParameter("password")).thenReturn("test");
        when(request.getParameter("email")).thenReturn("test");
        HttpSession httpSession = getProxyHttpSession();
        when(request.getSession()).thenReturn(httpSession);


        SignInServlet signInServlet = new SignInServlet(( e, p ) -> false);
        Assertions.assertDoesNotThrow(() -> signInServlet.doPost(request, response));
    }


    private HttpSession getProxyHttpSession() {
        Thread thread = Thread.currentThread();
        ClassLoader ccl = thread.getContextClassLoader();
        InvocationHandler handler = ( proxy, method, args1 ) -> {
            if (method.getName().equals("getAttribute"))
                return "Attribute";

            return null;
        };

        Class[] interfacesToImplement = {HttpSession.class};
        Object obj = Proxy.newProxyInstance(ccl, interfacesToImplement, handler);
        return (HttpSession) obj;
    }

    private class RequestDispatcherMock implements RequestDispatcher {
        int callCount = 0;

        @Override
        public void forward( ServletRequest servletRequest, ServletResponse servletResponse ) throws ServletException, IOException {
            Assertions.assertEquals(callCount++, 0);
            Assertions.assertEquals(servletRequest, request);
            Assertions.assertEquals(servletResponse, response);
        }

        @Override
        public void include( ServletRequest servletRequest, ServletResponse servletResponse ) throws ServletException, IOException {
            Assertions.fail();
        }
    }
}