package com.progressoft.jip8.app;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class SignInServlet extends HttpServlet {
    private SignInAuthentication signInAuthentication;


    public SignInServlet(SignInAuthentication signInAuthentication) {
        this.signInAuthentication = signInAuthentication;
    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        if (session.getAttribute("email") != null) {
            resp.sendRedirect("/reconcile");
            return;
        }

        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/index.jsp");
        requestDispatcher.forward(req, resp);

    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String password = req.getParameter("password");
        String email = req.getParameter("email");

        if (email != null && password != null && signInAuthentication.isAuthorized(email, password)) {
            handleAuthenticatedUser(req, resp, email);
            return;
        }
        handleAuthenticationFailure(req, resp);
        return;

    }

    private void handleAuthenticationFailure(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        session.setAttribute("signInFailed", "invalid user name or password.");
        RequestDispatcher invalidRequestDispatcher = req.getRequestDispatcher("/index.jsp");
        invalidRequestDispatcher.forward(req, resp);
    }

    private void handleAuthenticatedUser(HttpServletRequest req, HttpServletResponse resp, String email) throws IOException {
        HttpSession session = req.getSession();
        session.setAttribute("email", email);
        session.removeAttribute("reconcileFailed");

        session.setAttribute("step", "sign_In");
        resp.sendRedirect("/reconcile");
    }
}
