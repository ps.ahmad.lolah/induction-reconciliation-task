package com.progressoft.jip8.app;

import com.progressoft.jip8.app.zip.ZipBuilder;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

public class DownloadServlet extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        String step = (String) session.getAttribute("step");
        if (!step.equals("result")) {
            response.sendRedirect("/reconcile");
            return;
        }


        String requestedFormat = request.getParameter("format");
        String resultDirectory = (String) session.getAttribute(requestedFormat);
        if (resultDirectory == null) {
            session.setAttribute("reconcileFailed", "something went wrong.");
            request.getRequestDispatcher("/reconcile").forward(request, response);
            return;
        }

        downloadZip(response, resultDirectory);
    }

    private void downloadZip(HttpServletResponse response, String resultDirectory) throws IOException {
        File dir = Paths.get(resultDirectory).toFile();
        byte[] zip = ZipBuilder.makeZip(dir);


        ServletOutputStream sos = response.getOutputStream();
        response.setContentType("application/zip");
        response.setHeader("Content-Disposition", "attachment; filename= result.zip");

        sos.write(zip);
        sos.flush();
        sos.close();
    }
}