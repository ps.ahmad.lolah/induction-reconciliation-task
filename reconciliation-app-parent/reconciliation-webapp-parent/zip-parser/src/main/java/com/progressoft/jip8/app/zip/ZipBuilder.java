package com.progressoft.jip8.app.zip;

import java.io.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

// TODO zip file creator, builder, genreator "DONE"
public class ZipBuilder {
    private static final String FILE_SEPARATOR = System.getProperty("file.separator");

    public static byte[] makeZip(File directory) throws IOException {
        String[] files = directory.list();
        for (String file : files) {
            System.out.println(file);
        }
        if (isNotEmpty(files)) {
            return zipFiles(directory, files);
        }

        throw new IllegalArgumentException("failed to create zip file");
    }

    private static boolean isNotEmpty(String[] files) {
        return files != null && files.length > 0;
    }

    private static byte[] zipFiles(File directory, String[] files) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ZipOutputStream zos = new ZipOutputStream(baos);
        byte bytes[] = new byte[2048];

        for (String fileName : files) {
            FileInputStream fis = new FileInputStream(directory.getPath() +
                    ZipBuilder.FILE_SEPARATOR + fileName);
            BufferedInputStream bis = new BufferedInputStream(fis);

            zos.putNextEntry(new ZipEntry(fileName));

            int bytesRead;
            while ((bytesRead = bis.read(bytes)) != -1) {
                zos.write(bytes, 0, bytesRead);
            }
            zos.closeEntry();
            bis.close();
            fis.close();
        }
        zos.flush();
        baos.flush();
        zos.close();
        baos.close();

        return baos.toByteArray();
    }
}
