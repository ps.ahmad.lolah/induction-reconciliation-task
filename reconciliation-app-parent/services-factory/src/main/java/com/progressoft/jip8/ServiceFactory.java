package com.progressoft.jip8;

import com.progressoft.jip8.csv.BankTransactionsCSVFiller;
import com.progressoft.jip8.csv.CSVRecordValidator;
import com.progressoft.jip8.csv.CSVRecordsService;
import com.progressoft.jip8.json.JSONRecordValidator;
import com.progressoft.jip8.json.JSONRecordsService;
import com.progressoft.jip8.json.OnlineBankingTransactionJSONFiller;
import com.progressoft.jip8.validators.AmountValidatorByValue;
import com.progressoft.jip8.validators.ColumnLengthValidator;
import com.progressoft.jip8.validators.DateValidatorUsingDateFormat;
import org.json.simple.JSONObject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class ServiceFactory {
    private static Map<String, Factory> factories = new HashMap<>();

    static {
        factories.put("csv", ServiceFactory::newCSVRecordsService);
        factories.put("json", ServiceFactory::newJSONRecordsFactory);
    }

    public static RecordsService newService( Path path, String extension ) throws IOException {
        Objects.requireNonNull(extension, "null format");

        Factory factory = factories.getOrDefault(extension.toLowerCase(), ( p ) -> {
            throw new IllegalArgumentException("invalid extension " + extension);
        });
        return factory.newService(path);
    }

    private static RecordsService newJSONRecordsFactory( Path path ) throws FileNotFoundException {
        JSONRecordValidator jsonValidator =
                new JSONRecordValidator(new DateValidatorUsingDateFormat("dd/mm/yyyy"), new AmountValidatorByValue());

        Filler<JSONObject> jsonFiller = new OnlineBankingTransactionJSONFiller();

        return new JSONRecordsService(path, jsonFiller, jsonValidator);
    }

    private static RecordsService newCSVRecordsService( Path path ) throws IOException {
        CSVRecordValidator csvValidator = new CSVRecordValidator.RecordValidatorBuilder()
                .setAmountValidator(new AmountValidatorByValue(), 2)
                .setColumnValidator(new ColumnLengthValidator(7))
                .setDateValidator(new DateValidatorUsingDateFormat("yyyy-mm-dd"), 5)
                .build();

        Filler<String> csvFiller = new BankTransactionsCSVFiller();

        return new CSVRecordsService(path, csvFiller, csvValidator);
    }

    interface Factory {
        RecordsService newService( Path path ) throws IOException;
    }
}
