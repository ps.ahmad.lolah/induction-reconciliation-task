package com.progressoft.jip8;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ServiceFactoryTest {
    @Test
    public void givenNullFormat_whenGetNewService_thenThrowNulPointerException() {
        NullPointerException exception =
                Assertions.assertThrows(NullPointerException.class, () -> ServiceFactory.newService(Paths.get("."), null));
        Assertions.assertEquals("null format", exception.getMessage());
    }

    @Test
    public void givenNullPath_whenGetNewService_thenThrowNullPointerException() {
        NullPointerException exception =
                Assertions.assertThrows(NullPointerException.class, () -> ServiceFactory.newService(null, "CSV"));
        Assertions.assertEquals("null path", exception.getMessage());
    }

    @Test
    public void givenInvalidFormat_whenGetNewService_thenThrowIllegalArgumentException() throws IOException {
        IllegalArgumentException exception =
                Assertions.assertThrows(IllegalArgumentException.class, () -> ServiceFactory.newService(Paths.get("."), "BlaBla"));

        Assertions.assertEquals("invalid extension BlaBla", exception.getMessage());

    }

    @Test
    public void givenValidFormatAndPath_whenGetNewService_thenWorkAsExpected() throws IOException {
        RecordsService csv = ServiceFactory.newService(Files.createTempFile("Data", ".csv"), "CSV");
        Assertions.assertEquals(csv.getClass().getSimpleName(), "CSVRecordsService");
        RecordsService json = ServiceFactory.newService(Files.createTempFile("Data", ".json"), "JSON");
        Assertions.assertEquals(json.getClass().getSimpleName(), "JSONRecordsService");


    }
}
